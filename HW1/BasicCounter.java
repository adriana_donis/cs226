/*
Adriana Donis Noriega
adonisn1@jhu.edu
HW1
*/

/** Basic counter, initialization with value = 0, increase and decrease by 1. */
public class BasicCounter implements ResetableCounter {

    private int val;

    /** Constructor: sets the value to 0.  */
    public BasicCounter() {
        this.val = 0;
    }

    /** Returns value of counter.
    @return int
    */
    public int value() {
        return this.val;
    }

    /** Increases the value of counter by 1. */
    public void up() {
        this.val++;
    }

    /** Decreases the value of counter by 1. */
    public void down() {
        this.val--;
    }

    /** Resets the value of the counter to 0. */
    public void reset() {
        this.val = 0;
    }

    /** Tests the functionality of the counter.
    * @param args arguments
    */
    public static void main(String[] args) {
        ResetableCounter c = new BasicCounter();
        assert c.value() == 0;
        c.up();
        c.up();
        assert c.value() == 2;
        c.down();
        assert c.value() == 1;
        c.down();
        c.down();
        assert c.value() == -1;
        c.reset();
        assert c.value() == 0;
        c.up();
        assert c.value() == 1;
        System.out.println("ALL TESTS PASSED");
    }
}

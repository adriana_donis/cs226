/*
Adriana Donis Noriega
adonisn1@jhu.edu
HW1
*/

/** Class extends the ResetableCounter. It allows the programmer to
choose the initial value and the additive.*/
public class FlexibleCounter implements ResetableCounter {

    private int initialVal;
    private int val;
    private int additive;

    /** Driver constructor.
    * @param v initial value
    * @param a additive */
    public FlexibleCounter(int v, int a) {
        this.initialVal = v;
        this.val = v;
        this.additive = a;
    }

    /** Returns the value.
    @return int current value */
    public int value() {
        return this.val;
    }

    /** Increases the value of the counter by the specified amount.*/
    public void up() {
        this.val = this.val + this.additive;
    }

    /** Decreases the value of the counter by the specified amount. */
    public void down() {
        this.val = this.val - this.additive;
    }

    /** Resets the counter to the original value. */
    public void reset() {
        this.val = this.initialVal;
    }

    /** Set of tests using assertions.
    @param args command line arguments*/
    public static void main(String[] args) {
        ResetableCounter c = new FlexibleCounter(5, 3);
        assert c.value() == 5;
        c.up();
        c.up();
        assert c.value() == 11;
        c.down();
        c.down();
        c.down();
        assert c.value() == 2;
        c.reset();
        assert c.value() == 5;
        c.up();
        assert c.value() == 8;
        System.out.println("All tests passed");
    }
}
/* @author Adriana Donis Noriega
* adonisn1@jhu.edu
* HW1
*/

/** A counter that can me reset. */
public interface ResetableCounter extends Counter {
    /** Resets this counter. */
    void reset();
}

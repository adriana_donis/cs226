/*
Adriana Donis Noriega
adonisn1@jhu.edu
HW1
*/

/** Counter which starts at 2. When increased, the new value is the square
of the previous one. When decreased, the new value is the square root
of the old one.*/
public class SquareCounter implements ResetableCounter {

    private int val;

    /** Constructor. Initializes the counter to 2.*/
    public SquareCounter() {
        this.val = 2;
    }

    /** Returns the current value of the counter.
    @return int value*/
    public int value() {
        return this.val;
    }

    /** Squares the current value of the counter. */
    public void up() {
        this.val = this.val * this.val;
    }

    /** Takes the square root of the current value of the counter. */
    public void down() {
        this.val = (int) Math.ceil(Math.sqrt(this.val));
    }

    /** Resets the counter to its original value (2). */
    public void reset() {
        this.val = 2;
    }

    /** Tests the functionality of the class using assertions.
    @param args Command line arguments (ignored) */
    public static void main(String[] args) {
        ResetableCounter c = new SquareCounter();
        assert c.value() == 2;
        c.up();
        c.up();
        c.up();
        assert c.value() == 256;
        c.down();
        assert c.value() == 16;
        c.down();
        c.down();
        assert c.value() == 2;
        c.down();
        assert c.value() == 2;
        c.up();
        c.up();
        c.reset();
        assert c.value() == 2;
        c.up();
        assert c.value() == 4;
        System.out.println("ALL TESTS PASSED");
    }
}

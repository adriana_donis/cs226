/*
Adriana Donis Noriega
adonisn1
HW1
*/

/** Prints out a list of the Unique values that are given in
* the command line.
*/
public class Unique {

    /**
    * Default constructor.
    */
    public Unique() {

    }

    /** Prints an array of ints.
    * @param array array of ints to print
    * @param index number of integers that need to be printed. */
    public void printArray(int[] array, int index) {
        for (int i = 0; i < index; i++) {
            System.out.println(array[i]);
        }
    }

    /**
    * First it converts the String array to an Integer array. If there is an
    * exception (one of the arguments is not an integer), the program will stop
    * and print out that the arguments must be integers.
    *
    * Then it iterates over the array of integers (intarr) and checks if
    * the value is repeated along the array. If it is unique, it adds it to
    * the array toprint.
    *
    * After it finishes, the program prints the values in the array toprint
    *
    * @param args Command line arguments. If not all are integers, it stops.
    */
    public static void main(String[] args) {
        Unique u = new Unique();
        int[] intarr = new int[args.length];
        int[] toprint = new int[args.length];
        int index = 0;
        boolean haszero = false;
        boolean validArguments = true;
        int k = 0;
        for (int i = 0; i < args.length; i++) {
            try {
                intarr[k] = Integer.parseInt(args[i]);
                k++;
            } catch (NumberFormatException e) {
                System.out.print("");
            }
        }
        for (int i = 0; i < k; i++) {
            boolean repeated = false;
            for (int j = 0; j < toprint.length; j++) {
                if (intarr[i] == 0 && !haszero) {
                    haszero = true;
                    toprint[index] = 0;
                    index++;
                } else if (intarr[i] == toprint[j]) {
                    repeated = true;
                }
            }
            if (!repeated && intarr[i] != 0) {
                toprint[index] = intarr[i];
                index++;
            }
        }
        u.printArray(toprint, index);
    }
}
/**
Adriana Donis Noriega
adonisn1@jhu.edu
HW2
*/

import java.util.ArrayList; // see note in main() below

/**
    Simple polymorphic test framework for arrays.

    See last week's PolyCount. You need to add more test cases (meaning more
    methods like testNewLength and testNewWrongLength below) to make sure all
    preconditions and axioms are indeed as expected from the specification.
*/
public final class PolyArray {
    private static final int LENGTH = 113;
    private static final int INITIAL = 7;
    private static final int CHANGETO = 15;

    private PolyArray() {}

    // methods for testing axioms go here

    private static void testNewLength(Array<Integer> a) {
        assert a.length() == LENGTH;
    }

    // get(new(n, t), i) = t (initial value)
    private static void testNewGet(Array<Integer> a) {
        for (int i = 0; i < LENGTH; i++) {
            assert a.get(i) == INITIAL;
        }
    }

    // get(put(a, i, t), j) = (if i = j then t else get(a, j))
    private static void testGetPut(Array<Integer> a) {
        a.put(LENGTH / 2, CHANGETO);
        // if i = j then t
        assert a.get(LENGTH / 2) == CHANGETO;
        // else get(a, j)
        assert a.get(0) == INITIAL;
    }

    private static void testLengthPut(Array<Integer> a) {
        assert a.length() == LENGTH;
        a.put(LENGTH - 1, CHANGETO);
        assert a.length() == LENGTH;
    }

    // test iterator "iterates" over all values
    private static void testIterator(Array<Integer> a) {
        int total = 0;
        for (Integer i : a) {
            total++;
        }
        assert total == LENGTH;
    }

    // tests the iterator returns the values in the correct order
    // according to the position. (even sparse array)
    private static void testIteratorOrder() {
        ArrayList<Array<Integer>> arrays = new ArrayList<>();
        arrays.add(new SimpleArray<Integer>(5, 1));
        arrays.add(new ListArray<Integer>(5, 1));
        arrays.add(new SparseArray<Integer>(5, 1));
        int[] expected = {1, 1, 5, 1, 1};
        for (Array<Integer> a : arrays) {
            a.put(2, 5);
        }
        for (Array<Integer> ar : arrays) {
            int[] got = new int[5];
            int count = 0;
            for (Integer i : ar) {
                got[count++] = i;
            }
            boolean same = true;
            for (int i = 0; i < 5; i++) {
                if (expected[i] != got[i]) {
                    same = false;
                }
            }
            assert same;
        }
    }

    // methods for testing preconditions go here

    private static void testNewWrongLength() {
        try {
            Array<Integer> a = new SimpleArray<>(0, INITIAL);
            assert false;
        } catch (LengthException e) {
            // passed the test, nothing to do
        }
        try {
            Array<Integer> a = new ListArray<>(0, INITIAL);
            assert false;
        } catch (LengthException e) {
            // passed the test, nothing to do
        }
        try {
            Array<Integer> a = new SparseArray<>(0, INITIAL);
            assert false;
        } catch (LengthException e) {
            //passed the test, nothing to do
        }
    }

    // test get index less than zero. out of range
    private static void testGetLessThanZero() {
        ArrayList<Array<Integer>> arrays = new ArrayList<>();
        arrays.add(new SimpleArray<Integer>(LENGTH, INITIAL));
        arrays.add(new ListArray<Integer>(LENGTH, INITIAL));
        arrays.add(new SparseArray<Integer>(LENGTH, INITIAL));
        for (Array<Integer> a : arrays) {
            try {
                a.get(-1);
                assert false;
            } catch (IndexException e) {
                // passed the test, nothing to do
            }
        }
    }

    /** get(a, i): 0 <= i < length(a)
    Test length greater or equal to length trhows an index exception
    (out of range)*/
    private static void testGetGtoeLength() {
        ArrayList<Array<Integer>> arrays = new ArrayList<>();
        arrays.add(new SimpleArray<Integer>(LENGTH, INITIAL));
        arrays.add(new ListArray<Integer>(LENGTH, INITIAL));
        arrays.add(new SparseArray<Integer>(LENGTH, INITIAL));
        for (Array<Integer> a : arrays) {
            try {
                a.get(LENGTH);
                assert false;
            } catch (IndexException e) {
                // passed the test, nothing to do
            }
        }
    }

    // testing put less than zero (out of bounds)
    private static void testPutLessThanZero() {
        ArrayList<Array<Integer>> arrays = new ArrayList<>();
        arrays.add(new SimpleArray<Integer>(LENGTH, INITIAL));
        arrays.add(new ListArray<Integer>(LENGTH, INITIAL));
        arrays.add(new SparseArray<Integer>(LENGTH, INITIAL));
        for (Array<Integer> a : arrays) {
            try {
                a.put(-1, 5);
                assert false;
            } catch (IndexException e) {
                // passed the test, nothing to do
            }
        }
    }

    // testing put out of the bounds.
    private static void testPutGtoeLength() {
        ArrayList<Array<Integer>> arrays = new ArrayList<>();
        arrays.add(new SimpleArray<Integer>(LENGTH, INITIAL));
        arrays.add(new ListArray<Integer>(LENGTH, INITIAL));
        arrays.add(new SparseArray<Integer>(LENGTH, INITIAL));
        for (Array<Integer> a : arrays) {
            try {
                a.put(LENGTH, 5);
                assert false;
            } catch (IndexException e) {
                // passed the test, nothing to do
            }
        }
    }
    /**
        Run (mostly polymorphic) tests on various array implementations.

        Make sure you run this with -enableassertions! We'll learn a much
        better approach to unit testing later.

        @param args Command line arguments (ignored).
    */
    public static void main(String[] args) {
        // For various technical reasons, we cannot use a plain Java array here
        // like we did in PolyCount. Sorry.
        ArrayList<Array<Integer>> arrays = new ArrayList<>();
        arrays.add(new SimpleArray<Integer>(LENGTH, INITIAL));
        arrays.add(new ListArray<Integer>(LENGTH, INITIAL));
        arrays.add(new SparseArray<Integer>(LENGTH, INITIAL));

        // Test all the axioms. We can do that nicely in a loop. In the test
        // methods, keep in mind that you are handed the same object over and
        // over again!
        for (Array<Integer> a: arrays) {
            testNewLength(a);
            testNewGet(a);
            testIterator(a);
            testGetPut(a);
            testLengthPut(a);
        }

        testIteratorOrder();

        // Test all the preconditions. Sadly we have to code each one of these
        // out manually, not even Java's reflection API would help...
        testNewWrongLength();
        testGetLessThanZero();
        testGetGtoeLength();
        testPutLessThanZero();
        testPutGtoeLength();
    }
}
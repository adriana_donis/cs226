/** Adriana Donis Noriega
adonisn1@jhu.edu
HW2 - Part 3 */

import java.util.Iterator;

/** -
    Array implementation using a linked list.

    This implementation is used for arrays in which very few values differ
    from the original ones. It only stores the values for the positions
    that have been changed.

    There are two reasons for this class to exist: it helps avoid the wasteful
    storage of values in all positions since most don't change.

    @param <T> Element type.
*/
public class SparseArray<T> implements Array<T> {

    private class ListIterator implements Iterator<T> {
        // Current position in the basic Java array.
        Node<T> current;

        int iterated;

        ListIterator() {
            this.current = SparseArray.this.head;
        }

        @Override
        public T next() {
            Node<T> n = this.find(this.iterated);
            if (n != null) {
                this.iterated++;
                return n.data;
            } else {
                this.iterated++;
                return SparseArray.this.initial;
            }
            /*
            if (this.current != null) {
                T t = this.current.data;
                this.current = this.current.next;
                this.iterated++;
                return t;
            } else {
                this.iterated++;
                return SparseArray.this.initial;
            } */
        }

        public Node<T> find(int index) {
            if (index < 0 || index >= SparseArray.this.size) {
                throw new IndexException();
            }
            Node<T> n = SparseArray.this.head;
            int i = 0;
            try {
                while (n != null && i < SparseArray.this.size) {
                    if (n.position == index) {
                        return n;
                    }
                    n = n.next;
                    i++;
                }
                return n;
            } catch (NullPointerException e) {
                return SparseArray.this.head;
            }
        }

        // even though the linked list reaches null, the sparse array in general
        // will be the size with which it was initialized

        @Override
        public boolean hasNext() {
            if (SparseArray.this.size > this.iterated) {
                return true;
            }
            return false;
            // return this.current != null;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private static class Node<T> {
        private T data;
        private int position;
        private Node<T> next;

        Node(T t, int pos) {
            this.data = t;
            this.position = pos;
        }

        public T getData() {
            return this.data;
        }
    }

    // Length of linked list
    private static int length;
    // Initial value
    private T initial;
    // Size of Sparse Array
    private int size;
    // List of nodes
    private Node<T> head;

    /** -
        Constructs a new SparseArray.

        @param l Length of array, must be n &gt; 0.
        @param t Default value to store in each slot.
        @throws LengthException if n &le; 0.
    */
    public SparseArray(int l, T t) throws LengthException {
        this.initial = t;
        this.size = l;
        if (l <= 0) {
            throw new LengthException();
        }
        // this.data = (T[]) new Object[n];
    }

    /** Inserts a node at the beginning of the linked list
    @param t value to store
    @param position position in array */
    private void prepend(T t, int position) {
        Node<T> n = new Node<T>(t, position);
        // n.data = t;
        // n.position = position;
        n.next = this.head;
        this.head = n;
    }

    /** Prints the linked list in the format
    value at position x. */
    public void printList() {
        Node<T> cur = this.head;
        for (int i = 0; i < length; i++) {
            System.out.println(cur.data + " at position " + cur.position);
            cur = cur.next;
        }
    }

    @Override
    public void put(int i, T t) throws IndexException {
        if (t != this.initial) {
            Node<T> cur = this.find(i);
            if (cur == null) {
                this.prepend(t, i);
                length++;
            } else {
                cur.data = t;
            }
        }
    }

    /** Find the node for a given position. It checks that the index
    is within the range. If the node with that position doesn't exist
    it returns the head.
    @param index position at which certain value should be stored

    @return Node node at given position. */
    public Node<T> find(int index) {
        if (index < 0 || index >= this.size) {
            throw new IndexException();
        }
        Node<T> n = this.head;
        int i = 0;
        try {
            while (n != null && i < this.size) {
                if (n.position == index) {
                    return n;
                }
                n = n.next;
                i++;
            }
            return n;
        } catch (NullPointerException e) {
            return this.head;
        }
    }

    // TO DO
    @Override
    public T get(int i) throws IndexException {
        try {
            Node<T> n = this.find(i);
            return n.data;
        } catch (NullPointerException e) {
            return this.initial;
        }
    }

    @Override
    public int length() {
        return this.size;
    }

    @Override
    public Iterator<T> iterator() {
        return new ListIterator();
    }
}
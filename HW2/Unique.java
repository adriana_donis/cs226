/** Adriana Donis Noriega
adonisn1@jhu.edu
HW2
*/

import java.util.Scanner;
/** -
    This program allows the user to input as many integers as they want.
    (it ignores invalid inputs such as strings) Then it prints out the
    Unique values.
*/

public final class Unique {
    // array of unique numbers
    private static Array<Integer> data;
    // how many slots in data are used?
    private static int used;
    // Array of inputs
    private static Array<Integer> myArray;
    // Total number of valid inputs
    private static int totalInputs;
    // Initial length of array
    private static int length = 10;

    // make checkstyle happy
    private Unique() {}

    // position of given value in data array, -1 if not found
    private static int find(int value) {
        for (int i = 0; i < used; i++) {
            if (data.get(i).equals(value)) {
                return i;
            }
        }
        return -1;
    }

    // insert value into data array if not already present
    private static void insert(int value) {
        int position = find(value);
        if (position < 0) {
            data.put(used, value);
            used += 1;
        }
    }

    // reads the input into an array of Integers
    private static void readInput() {
        Scanner scan = new Scanner(System.in);
        myArray = new SimpleArray<Integer>(length, 0);
        while (scan.hasNext()) {
            if (totalInputs < length) {
                String arg = scan.next();
                try {
                    int i = Integer.parseInt(arg);
                    myArray.put(totalInputs, i);
                    totalInputs++;
                } catch (NumberFormatException e) {
                    System.err.printf("Ignored non-integer argument.\n");
                }
            } else {
                // copies to a temporary array
                Array<Integer> tempArray = new SimpleArray<Integer>(length, 0);
                for (int i = 0; i < length; i++) {
                    tempArray.put(i, myArray.get(i));
                }
                length = length * 2;
                myArray = new SimpleArray<Integer>(length, 0);
                // copies from temporary array to new array.
                for (int i = 0; i < length / 2; i++) {
                    myArray.put(i, tempArray.get(i));
                }
            }
        }
    }
    /** -
        Print only unique integers out of given command line arguments.

        @param args Command line arguments (ignored)
    */
    public static void main(String[] args) {
        //readInput();
        data = new SimpleArray<Integer>(length, 0);
        readInput();
        // process args and insert unique numbers into data[]
        for (int j = 0; j < totalInputs; j++) {
            insert(myArray.get(j));
        }
        // output unique numbers in array order
        for (int i = 0; i < used; i++) {
            System.out.println(data.get(i));
        }
    }
}
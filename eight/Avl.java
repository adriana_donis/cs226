/**
* @author Adriana Donis Noriega
* adonisn1@jhu.edu
* HW8
*/

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

public class Avl<K extends Comparable<? super K>, V>
    implements OrderedMap<K, V> {

    private class Node {

		Node left;
		Node right;
		K key;
		V value;
		int height;

		Node(K k, V v) {
		    this.key = k;
		    this.value = v;
		}

		public void updateHeight(int diff, boolean modify) {
			if (modify) {

			}
		}

		public String toString() {
			String s = "Node: " + this.key + " H: " + this.height + " ";
			if (this.left != null) {
				s += "LEFT: " + this.left.key + " ";
			}
			if (this.right != null) {
				s += "RIGHT : " + this.right.key + " ";
			}
			s += "\n";
			return s;
		} 
	}

	private Node root;
	private int size;
	private StringBuilder stringBuilder;
	private boolean update;

	private int height(Node n) {
    	if (n == null) {
    		return -1;
    	}
    	return n.height;
    }

    @Override
    public int size() {
        return this.size;
    }

    private Node find(K k) {
        if (k == null) {
            throw new IllegalArgumentException("cannot handle");
        }
        Node n = this.root;
        while (n != null) {
            int cmp = k.compareTo(n.key);
            if (cmp < 0) {
                n = n.left;
            } else if (cmp > 0) {
                n = n.right;
            } else {
                return n;
            }
        }
        return null;
    }

    @Override
    public boolean has(K k) {
        if (k == null) {
            return false;
        }
        return this.find(k) != null;
    }

    private Node findForSure(K k) {
        Node n = this.find(k);
        if (n == null) {
            throw new IllegalArgumentException("cannot find key " + k);
        }
        return n;
    }

    @Override
    public void put(K k, V v) {
        Node n = this.findForSure(k);
        n.value = v;
    }

    @Override
    public V get(K k) {
        Node n = this.findForSure(k);
        return n.value;
    }

    @Override
    public void insert(K k, V v) {
        this.update = true;
        if (k == null) {
            throw new IllegalArgumentException("cannot handle null key");
        }
        this.root = this.insert(this.root, k, v);
        this.size += 1;
    }

    private Node insert(Node n, K k, V v) {
        if (n == null) {
            return new Node(k, v);
        }
        int cmp = k.compareTo(n.key);
        if (cmp < 0) {
            n.left = this.insert(n.left, k, v);
            n.height = Math.max(this.height(n.left), this.height(n.right)) + 1;
            if (checkBalance(n, n.right) > 2) {
            	//System.out.println("ROTATE");
            	n = typeOfRotation(n);
            }
        } else if (cmp > 0) {
            n.right = this.insert(n.right, k, v);
            n.height = Math.max(this.height(n.left), this.height(n.right)) + 1;
            if (checkBalance(n, n.left) > 2) {
            	n = typeOfRotation(n);
            }
        } else {
            throw new IllegalArgumentException("duplicate key " + k);
        }

        return n;
    }

    private int checkBalance(Node n, Node other) {
    	if (n == null) { 
    		return 0;
    	}
    	return this.height(n) - this.height(other);
    }

    private int sideUnbalanced(Node n) {
    	if (height(n.left) > height(n.right)) {
    		return -1;
    	} else if (height(n.left) == height(n.right)) {
    		return 0;
    	} else {
    		return 1;
    	}
    }

    private Node typeOfRotation(Node n) {
    	int first;
    	int second;
    	first = sideUnbalanced(n);
    	if (first > 0) {
    		second = sideUnbalanced(n.right);
    	} else {
    		second = sideUnbalanced(n.left);
    	}
   
    	if (first > 0 && second > 0) {
    		return this.singleLeftRotation(n);
    	} else if (first > 0 && second < 0) {
    		n.right = this.singleRightRotation(n.right);
    		n = this.singleLeftRotation(n);
    		n.right.height++;
    		return n;
    	} else if (first < 0 && second < 0) {
    		return this.singleRightRotation(n);
    	} else if (first < 0 && second > 0) {
    		n.left = this.singleLeftRotation(n.left);
    		n = this.singleRightRotation(n);
    		n.left.height++;
    		return n;
    	} else {
    		return n;
    	}
    }

    private Node singleRightRotation(Node n) {
    	Node p = n.left;
    	p.height = n.height - 1;
    	n.height = n.left.height - 1;
        n.left = p.right;
        p.right = n;
        return p;
    }

    private Node singleLeftRotation(Node n) {
        Node p = n.right;
        p.height = n.height - 1;
        n.height = n.right.height - 1;
        n.right = p.left;
        p.left = n;
        return p;
    }

    private void copyValues(Node from, Node to) {
        to.key = from.key;
        to.value = from.value;
        to.left = this.remove(to.left, from.key);
    }

    private Node remove(Node n) {
        // 0 and 1 child
        if (n.left == null) {
            return n.right;
        }
        if (n.right == null) {
            return n.left;
        }

        // 2 children
        Node max = this.max(n.left);
        this.copyValues(max, n);
        return n;
    }

    private Node remove(Node n, K k) {
    	if (n == null) {
    		throw new IllegalArgumentException("cannot find key " + k);
    	}
    	int cmp = k.compareTo(n.key);
    	if (cmp < 0) {
    		n.left = this.remove(n.left, k);
            n.height = Math.max(this.height(n.left), this.height(n.right)) + 1;
            if (checkBalance(n, n.right) > 2) {
            	n = typeOfRotation(n);
            }
    	} else if (cmp > 0) {
    		n.right = this.remove(n.right, k);
    		n.height = Math.max(this.height(n.left), this.height(n.right)) + 1;
    		if (checkBalance(n, n.right) > 2) {
            	n = typeOfRotation(n);
            }
    	} else {
    		n = this.remove(n);

    	}
    	return n;
    }
    @Override
    public V remove(K k) {
        // Need this additional find() for the V return value, because the
        // private remove() method cannot return that in addition to the new
        // root. If we had been smarter and used a void return type, we would
        // not need to do this extra work.
        Node n = this.find(k);
        this.root = this.remove(this.root, k);
        this.size -= 1;
        return n.value;
    }
    // Recursively append string representations of keys and
    // values from subtree rooted at given node.
    private void toStringHelper(Node n, StringBuilder s) {
        if (n == null) {
            return;
        }
        this.toStringHelper(n.left, s);
        s.append(n.toString());
        //s.append("\n");
        /*s.append(n.key);
        s.append(": ");
        s.append(n.value);
        s.append(", ");*/
        this.toStringHelper(n.right, s);
    }

    @Override
    public String toString() {
        this.setupStringBuilder();
        this.stringBuilder.append("{\n");

        this.toStringHelper(this.root, this.stringBuilder);

        int length = this.stringBuilder.length();
        if (length > 1) {
            // If anything was appended at all, get rid of
            // the last ", " the toStringHelper put in.
            this.stringBuilder.setLength(length - 2);
        }
        this.stringBuilder.append("}\n");

        return this.stringBuilder.toString();
    }

    // Return node with maximum key in subtree rooted
    // at given node. (Iterative version because once
    // again recursion has no advantage here.)
    private Node max(Node n) {
        while (n.right != null) {
            n = n.right;
        }
        return n;
    }

    // Recursively add keys from subtree rooted at given node
    // into the given list.
    private void iteratorHelper(Node n, List<K> keys) {
        if (n == null) {
            return;
        }
        this.iteratorHelper(n.left, keys);
        keys.add(n.key);
        this.iteratorHelper(n.right, keys);
    }

    @Override
    public Iterator<K> iterator() {
        List<K> keys = new ArrayList<K>();
        this.iteratorHelper(this.root, keys);
        return keys.iterator();
    }

    // If we don't have a StringBuilder yet, make one;
    // otherwise just reset it back to a clean slate.
    private void setupStringBuilder() {
        if (this.stringBuilder == null) {
            this.stringBuilder = new StringBuilder();
        } else {
            this.stringBuilder.setLength(0);
        }
    }


    public static void main(String args[]) {
    	Avl<Integer, String> map = new Avl<>();
    	map.insert(1, "");
    	map.insert(2, "");
    	map.insert(3, "");
    	map.insert(4, "");
    	map.insert(-1, "");
    	map.insert(0, "");
    	System.out.print(map);
    	map.remove(0);
    	map.remove(2);
    	System.out.print(map);

    	/*Avl<Integer, String> map2 = new Avl<>();
    	ArrayList<Integer> expected = new ArrayList<>();
	    for (int i = 0; i < 1000; i++) {
	    	map2.insert(i, "");
	    	expected.add(i);
	    }
	    int index = 0;
	    for (int i : map2) {
	    	if (!expected.get(index++).equals(i)) {
	    		System.out.println("error" + i);
	    		break;
	    	}
	    }
    	//System.out.print(map2);*/
    }


}
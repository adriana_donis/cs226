/**
* @author Adriana Donis Noriega
* adonisn1@jhu.edu
* HW8
*/

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

/**
* Ordered maps from comparable keys to arbitrary values.
*
* Balanced Tree that follows the AVL condition:
*  for every node n the height of n's left and right subtrees
*  differ by at most 1.
*
* To maintain the balance and order, rotations are performed 
* when necessary.
*
* Rule most be followed at all times: inserting and removing.
* @param <K> type for keys.
* @param <V> type for values.
*/

public class AvlTreeMap<K extends Comparable<? super K>, V>
    implements OrderedMap<K, V> {

    private class Node {

        Node left;
        Node right;
        K key;
        V value;
        int heightL; // stores the height of left side
        int heightR; // stores the height of right side

        Node(K k, V v) {
            this.key = k;
            this.value = v;
        }

        /**
        * If there has been a rotation, it doesn't need to update
        * the height.
        * If not it checks if an increase in the height would break
        * the rules of AVL trees, if so, it returns a negative value
        * if the left side is larger than the right one.
        * Positive if right is larger. and zero if it doesn't need
        * to be rotated.
        * @param inc value to modify height
        * @param rot if there has already been a rotation.
        * @return int positive, right is bigger
        */
        public int updateRightHeight(int inc, boolean rot, boolean rem) {
            if (rot || !rem) {
                return 0;
            }  else if (!rot && rem) {
                this.heightR += inc;
                return 0;
            }
            if (Math.abs(this.heightR + inc - this.heightL) <= 1) {
                this.heightR += inc;
                return 0;
            }
            return this.heightR - this.heightL;
        }

        public int updateRightHeightI(int inc, boolean rot) {
            if (rot) {
                return 0;
            }
            if (Math.abs(this.heightR + inc - this.heightL) <= 1) {
                this.heightR += inc;
                return 0;
            }
            return this.heightR - this.heightL;
        }

        /**
        * Same as updateRight, but makes the change in the left height.
        * @param inc value to modify height
        * @param rot if there has already been a rotation
        * @return int positive if height right is bigger.
        */
        public int updateLeftHeight(int inc, boolean rot, boolean rem) {
            if (rot && !rem) {
                return 0;
            } else if (!rot && rem) {
                this.heightL += inc;
                return 0;
            }
            if (Math.abs(this.heightL + inc - this.heightR) <= 1) {
                this.heightL += inc;
                return 0;
            }
            return this.heightR - this.heightL;
        }

        public int updateLeftHeightI(int inc, boolean rot) {
            if (rot) {
                return 0;
            }
            if (Math.abs(this.heightR + inc - this.heightL) <= 1) {
                this.heightR += inc;
                return 0;
            }
            return this.heightR - this.heightL;
        }

        /**
        * Prints the information of each node.
        * @return String with the key, value, and heights.
        */
        public String toString() {
            String s = "Node<key: " + this.key
                + "; value: " + this.value
                + ">" + "\nLeft Height: " + this.heightL + "\nRight Height:"
                + this.heightR;
            if (this.left != null) {
                s += "\nChild left " + this.left.key + " \n";
            }
            if (this.right != null) {
                s += " Child Right " + this.right.key + " \n";
            }
            return s;
        }
    }

    private Node root;
    private int size;
    private StringBuilder stringBuilder;
    private boolean rotated;
    private boolean updateH;

    @Override
    public int size() {
        return this.size;
    }

    /**
    * looks for a key in the map. Checks if it is bigger, it moves
    * to the right, else it moves to the left. If it is not found,
    * returns null.
    * @param k key that is being looked for
    * @return Node node that has that key value.
    */
    private Node find(K k) {
        if (k == null) {
            throw new IllegalArgumentException("cannot handle");
        }
        Node n = this.root;
        while (n != null) {
            int cmp = k.compareTo(n.key);
            if (cmp < 0) {
                n = n.left;
            } else if (cmp > 0) {
                n = n.right;
            } else {
                return n;
            }
        }
        return null;
    }

    @Override
    public boolean has(K k) {
        if (k == null) {
            return false;
        }
        return this.find(k) != null;
    }

    /**
    * If the value returned by find is null (not found, it throws an
    * exception)
    * @param k key that is being looked for
    * @return Node node that has that key value.
    */
    private Node findForSure(K k) {
        Node n = this.find(k);
        if (n == null) {
            throw new IllegalArgumentException("cannot find key " + k);
        }
        return n;
    }

    @Override
    public void put(K k, V v) {
        Node n = this.findForSure(k);
        n.value = v;
    }

    @Override
    public V get(K k) {
        Node n = this.findForSure(k);
        return n.value;
    }

    /**
    * Checks which side is unbalanced.
    * @param n parent node to which we are looing which side is unbalanced
    * @return int positive if right is taller, negative if left is taller
    */
    private int checkWhichUnbalanced(Node n) {
        if (n == null) {
            return 0;
        }
        if (n.heightL > n.heightR) {
            return -1;
        } else {
            return 1;
        }
    }

    /**
    * if it is positive, the right side is larger and a left rotation
    * needs to be made. Negative is the way around.
    * To check if it is single, or double rotation, checks which side
    * of the grandchildren is taller.
    * @param balance defines which side needs to be balanced.
    * @param n node around which the rotation will take place
    * @return Node new "root" from subtree
    */
    private Node typeOfRotation(int balance, Node n) {
        if (balance > 0) {
            if (this.checkWhichUnbalanced(n.right) > 0) {
                return this.singleLeftRotation(n);
            } else {
                n.right = this.singleRightRotation(n.right);
                return this.singleLeftRotation(n);
                //return this.doubleLeft(n);
            }
        } else if (balance < 0) {
            if (this.checkWhichUnbalanced(n.left) < 0) {
                return this.singleRightRotation(n);
            } else {
                n.left = this.singleLeftRotation(n.left);
                return this.singleRightRotation(n);
                //return this.doubleRight(n);
            }
        } else {
            return n;
        }
    }

    /**
    * First it checks where the new node must be put according to its key.
    * It then puts the node, and checks if increasing the height would
    * cause an unbalanced tree. Then it checks the type of rotation.
    * @param n node being compared
    * @param k key of new node
    * @param v value of new node
    * @return Node new inserted node, or new "root" of subtree if rotated
    */
    private Node insert(Node n, K k, V v) {
        if (n == null) {
            return new Node(k, v);
        }
        int cmp = k.compareTo(n.key);
        if (cmp < 0) {
            n.left = this.insert(n.left, k, v);
            int balance = n.updateLeftHeightI(1, this.rotated);
            n = this.typeOfRotation(balance, n);
        } else if (cmp > 0) {
            n.right = this.insert(n.right, k, v);
            int balance = n.updateRightHeightI(1, this.rotated);
            n = this.typeOfRotation(balance, n);
        } else {
            throw new IllegalArgumentException("duplicate key " + k);
        }

        return n;
    }

    @Override
    public void insert(K k, V v) {
        this.rotated = false;
        this.updateH = false;
        if (k == null) {
            throw new IllegalArgumentException("cannot handle null key");
        }
        this.root = this.insert(this.root, k, v);
        this.size += 1;
    }

    /**
    * If the tree's left side is larger. The parent becomes the
    * right of left child.
    * @param n node being rotated around
    * @return Node new "root" of subtree
    */
    private Node singleRightRotation(Node n) {
        this.rotated = true;
        Node p = n.left;
        n.left = p.right;
        p.right = n;
        p.heightR++;
        n.heightL--;
        return p;
    }

    /**
    * If the tree's right side is larger. The parent becomes the
    * left of right child.
    * @param n node being rotated around
    * @return Node new "root" of subtree
    */
    private Node singleLeftRotation(Node n) {
        this.rotated = true;
        Node p = n.right;
        n.right = p.left;
        p.left = n;
        p.heightL++;
        n.heightR--;
        return p;
    }

    /**
    * Copies the key and values from one node to another.
    * @param from node from which we are copying values from
    * @param to node from which we are copying values to
    */
    private void copyValues(Node from, Node to) {
        to.key = from.key;
        to.value = from.value;
        to.left = this.remove(to.left, from.key);
        to.heightL--;
    }


    // Remove node with given key from subtree rooted at
    // given node; return changed subtree with given key
    // missing. (Once again doing this recursively makes
    // it easier to add fancy rebalancing code later.)
    private Node remove(Node n, K k) {
        if (n == null) {
            throw new IllegalArgumentException("cannot find key " + k);
        }
        int cmp = k.compareTo(n.key);
        if (cmp < 0) {
            n.left = this.remove(n.left, k);
            if (n.left == null && n.right == null) {
                n.heightL = 0;
                n.heightR = 0;
                this.updateH = true;
            }
            int balance = n.updateLeftHeight(-1, this.rotated, this.updateH);
            n = this.typeOfRotation(balance, n);
        } else if (cmp > 0) {
            n.right = this.remove(n.right, k);

            if (n.left == null && n.right == null) {
                n.heightL = 0;
                n.heightR = 0;
                this.updateH = true;
            }
            int balance = n.updateRightHeight(-1, this.rotated, this.updateH);
            n = this.typeOfRotation(balance, n);
                
                //n.heightR--;
        } else {
            n = this.remove(n);
        }

        //System.out.println("Node to remove: " + n);
        return n;
    }

    // Remove given node and return the remaining tree.
    // Easy if the node has 0 or 1 child; if it has two
    // children, find the predecessor, copy its data to
    // the given node (thus removing the key we need to
    // get rid off), the remove the predecessor node.
    private Node remove(Node n) {
        // 0 and 1 child
        if (n.left == null) {
            return n.right;
        }
        if (n.right == null) {
            return n.left;
        }

        // 2 children
        Node max = this.max(n.left);
        this.copyValues(max, n);
        return n;
    }

    @Override
    public V remove(K k) {
        // Need this additional find() for the V return value, because the
        // private remove() method cannot return that in addition to the new
        // root. If we had been smarter and used a void return type, we would
        // not need to do this extra work.
        this.rotated = false;
        this.updateH = false;
        Node n = this.find(k);
        this.root = this.remove(this.root, k);
        this.size -= 1;
        return n.value;
    }

    // Recursively append string representations of keys and
    // values from subtree rooted at given node.
    private void toStringHelper(Node n, StringBuilder s) {
        if (n == null) {
            return;
        }
        this.toStringHelper(n.left, s);
        s.append(n.toString());
        s.append("\n");
        /*s.append(n.key);
        s.append(": ");
        s.append(n.value);
        s.append(", ");*/
        this.toStringHelper(n.right, s);
    }

    @Override
    public String toString() {
        this.setupStringBuilder();
        this.stringBuilder.append("{");

        this.toStringHelper(this.root, this.stringBuilder);

        int length = this.stringBuilder.length();
        if (length > 1) {
            // If anything was appended at all, get rid of
            // the last ", " the toStringHelper put in.
            this.stringBuilder.setLength(length - 2);
        }
        this.stringBuilder.append("}");

        return this.stringBuilder.toString();
    }

    // Return node with maximum key in subtree rooted
    // at given node. (Iterative version because once
    // again recursion has no advantage here.)
    private Node max(Node n) {
        while (n.right != null) {
            n = n.right;
        }
        return n;
    }

    // Recursively add keys from subtree rooted at given node
    // into the given list.
    private void iteratorHelper(Node n, List<K> keys) {
        if (n == null) {
            return;
        }
        this.iteratorHelper(n.left, keys);
        keys.add(n.key);
        this.iteratorHelper(n.right, keys);
    }

    @Override
    public Iterator<K> iterator() {
        List<K> keys = new ArrayList<K>();
        this.iteratorHelper(this.root, keys);
        return keys.iterator();
    }

    // If we don't have a StringBuilder yet, make one;
    // otherwise just reset it back to a clean slate.
    private void setupStringBuilder() {
        if (this.stringBuilder == null) {
            this.stringBuilder = new StringBuilder();
        } else {
            this.stringBuilder.setLength(0);
        }
    }
}
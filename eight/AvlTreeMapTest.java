/**
* Adriana Donis Noriega
* adonisn1@jhu.edu
* HW8
*/

/**
* Implements abstract method createMap() from MapTestBase.
*/
public class AvlTreeMapTest extends MapTestBase {

    @Override
    protected Map<Integer, String> createMap() {
        return new AvlTreeMap<Integer, String>();
    }
}
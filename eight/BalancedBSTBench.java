/**
* @author Adriana Donis Noriega
* adonisn1@jhu.edu
* HW8
*/

import com.github.phf.jb.Bench;
import com.github.phf.jb.Bee;
import java.util.ArrayList;
import java.util.Collections;

import java.util.Random;

/**
* Benchmark performance of AvlTreeMap.
*/
public final class BalancedBSTBench {
    private static final int SIZE = 1000000;
    private static final Random RAND = new Random();

    private BalancedBSTBench() {}

    // Insert consecutive strings into the map.
    private static void insertLinear(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.insert(Integer.toString(i), i);
        }
    }

    // Insert a number of "random" Integers into the given map.
    private static void insertRandom(
        Map<String, Integer> m, ArrayList<String> r) {
        for (int i = 0; i < SIZE; i++) {
            m.insert(r.get(i), i);
        }
    }

    private static void lookupLinear(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            boolean x = m.has(Integer.toString(i));
        }
    }

    private static void lookupRandom(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            boolean x = m.has(Integer.toString(RAND.nextInt(SIZE)));
        }
    }

    private static void removeRandom(
        Map<String, Integer> m, ArrayList<String> r) {
        for (int i = 0; i < SIZE; i++) {
            m.remove(r.get(i));
        }
    }

    private static void removeLinear(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.remove(Integer.toString(i));
        }
    }

    private static ArrayList<String> createUniqueNumbers() {
        ArrayList<String> n = new ArrayList<>();
        for (int i = 0; i < SIZE; i++) {
            n.add(Integer.toString(i));
        }
        Collections.shuffle(n);
        return n;
    }

    @Bench
    public static void insertLinearAVL(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            b.start();
            insertLinear(m);
        }
    }

    @Bench
    public static void insertRandomAVL(Bee b) {
        ArrayList<String> random = createUniqueNumbers();
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            b.start();
            insertRandom(m, random);
        }
    }

    @Bench
    public static void lookupLinearAVL(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            insertLinear(m);
            b.start();
            lookupLinear(m);
        }
    }

    @Bench
    public static void lookupRandomAVL(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            insertLinear(m);
            b.start();
            lookupRandom(m);
        }
    }

    @Bench
    public static void removeLinearAVL(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            insertLinear(m);
            b.start();
            removeLinear(m);
        }
    }

    @Bench
    public static void removeRandomAVL(Bee b) {
        ArrayList<String> random = createUniqueNumbers();
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            insertLinear(m);
            b.start();
            removeRandom(m, random);
        }
    }

    @Bench
    public static void ranInsertRanRemoveAVL(Bee b) {
        ArrayList<String> randomI = createUniqueNumbers();
        ArrayList<String> randomR = createUniqueNumbers();
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            insertRandom(m, randomI);
            b.start();
            removeRandom(m, randomR);
        }
    }

    @Bench
    public static void ranInsertlinRemoveAVL(Bee b) {
        ArrayList<String> randomI = createUniqueNumbers();
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new AvlTreeMap<>();
            insertRandom(m, randomI);
            b.start();
            removeLinear(m);
        }
    }
}
/**
* Adriana Donis Noriega.
* adonisn1@jhu.edu
* HW8
*/

public class BinarySearchTreeMapTest extends MapTestBase {

    @Override
    protected Map<Integer, String> createMap() {
        return new BinarySearchTreeMap<Integer, String>();
    }
}
/**
* @author Adriana Donis Noriega
* adonisn1@jhu.edu
* HW8
*/

import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public abstract class MapTestBase {

    int SIZE = 15;

    protected Map<Integer, String> map;

    protected abstract Map<Integer, String> createMap();

    @Before
    public void setupMap() {
        this.map = this.createMap();
    }

    @Test
    public void newSize() {
        assertEquals(0, map.size());
    }

    @Test
    public void newHas() {
        assertFalse(map.has(5));
    }

    @Test
    public void testInsert() {
        this.map.insert(10, "root");
        assertEquals(1, map.size());
    }

    @Test
    public void hasInsert() {
        this.map.insert(10, "root");
        assertTrue(this.map.has(10));
    }

    @Test
    public void insertGet() {
        this.map.insert(10, "root");
        assertEquals("root", this.map.get(10));
    }


    protected void populateMap() {
        for (int i = SIZE; i > 0; i--) {
            this.map.insert(i, Integer.toString(i));
        }
    }
    
    @Test
    public void sizeMap() {
        this.populateMap();
        assertEquals(SIZE, this.map.size());
    }

    @Test
    public void removeKey() {
        this.populateMap();
        this.map.remove(1);
        assertEquals(SIZE - 1, this.map.size());
        assertFalse(this.map.has(1));
    }

    @Test
    public void putValue() {
        this.populateMap();
        this.map.put(1, "first");
        assertEquals("first", this.map.get(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullPut() {
        this.map.put(null, "error");
    }

    @Test(expected = IllegalArgumentException.class)
    public void doesntExistPut() {
        this.populateMap();
        this.map.put(-5, "doesn't exist");
    }
    @Test(expected = IllegalArgumentException.class)
    public void insertExisting() {
        this.populateMap();
        this.map.insert(1, "already there");
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertNull() {
        this.map.insert(null, "can't insert null");
    }

    @Test
    public void hasNull() {
        this.populateMap();
        assertFalse(this.map.has(null));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getNotInMap() {
        populateMap();
        this.map.get(SIZE + 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeNotInMap() {
        populateMap();
        this.map.remove(SIZE + 1);
    }
    
    @Test
    public void singleRightRotation() {
        this.map.insert(5, "root");
        this.map.insert(4, "left");
        this.map.insert(3, "leftmost");
        this.map.insert(2, "all good");
        this.map.insert(1, "rotate again");
        this.map.insert(0, "another roation");
        this.map.insert(-1, "another roation");
        this.map.insert(-2, "rotate again");
        this.map.insert(-3, "another roation");
        this.map.insert(-4, "rotate again");
        this.map.insert(-5, "another roation");
        int index = 0;
        ArrayList<Integer> expected = buildExpected(-5, 5);
        for (Integer i : map) {
            assertEquals(expected.get(index++), i);
        }
        assertEquals(11, this.map.size());
    } 
    
    @Test
    public void doubleRightRotation() {
        this.map.insert(11, "root");
        this.map.insert(7, "left");
        this.map.insert(8, "right of left");
        this.map.insert(5, "left");
        this.map.insert(6, "six");
        this.map.insert(9, "seven");
        this.map.insert(10, "eight");
        int index = 0;
        ArrayList<Integer> expected = buildExpected(5, 11);
        for (Integer i : map) {
            assertEquals(expected.get(index++), i);
        }
    }

    @Test
    public void singleLeftRotation() {
        this.map.insert(1, "root");
        this.map.insert(2, "right");
        this.map.insert(3, "rightmost");
        this.map.insert(4, "more right");
        this.map.insert(5, "rotate");
        this.map.insert(6, "more right");
        this.map.insert(7, "rotate");
        this.map.insert(8, "more right");
        this.map.insert(9, "rotate");
        int index = 0;
        ArrayList<Integer> expected = buildExpected(1, 9);
        for (Integer i : map) {
            assertEquals(expected.get(index++), i);
        }
    }

    @Test
    public void doubleLeftRotation() {
        this.map.insert(1, "root");
        this.map.insert(5, "right");
        this.map.insert(2, "left of right");
        this.map.insert(-1, "");
        this.map.insert(0, "");
        this.map.insert(-2, "");
        this.map.insert(3, "");
        this.map.insert(4, "");

        int index = 0;

        ArrayList<Integer> expected = buildExpected(-2, 5);
        for (Integer i : map) {
            assertEquals(expected.get(index++), i);
        }
    }


    @Test
    public void rotateLeftAndRight() {
        ArrayList<Integer> expected = buildExpected(1, 9);
        this.map.insert(5, "more right");
        this.map.insert(4, "rotate");
        this.map.insert(6, "more right");
        this.map.insert(7, "rotate");
        this.map.insert(3, "more right");
        this.map.insert(2, "rotate");
        this.map.insert(1, "rotate");
        this.map.insert(8, "more right");
        int index = 0;
        for (Integer i : map) {
            assertEquals(expected.get(index++), i);
        }
    }

    private ArrayList<Integer> buildExpected(int from, int to) {
        ArrayList<Integer> array = new ArrayList<Integer>();
        for (int i = from; i <= to; i++) {
            array.add(i);
        }
        return array;
    }

    @Test
    public void testRemoveSmallEmpty() {
        this.map.insert(1, "");
        this.map.remove(1);
        this.map.insert(1, "");
        this.map.insert(2, "");
        this.map.insert(0, "");
        this.map.remove(1);
        this.map.remove(0);
        this.map.remove(2);
        assertEquals(0, this.map.size());
    }

    @Test
    public void testRemoveSingleRightRotation() {
        this.map.insert(1, "");
        this.map.insert(2, "");
        this.map.insert(-1, "");
        this.map.insert(-2, "");
        this.map.remove(2);
        this.map.insert(0, "");
        this.map.remove(1);
        this.map.remove(-1);
        this.map.remove(-2);
        int index = 0;
        assertFalse(this.map.has(2));
        assertEquals(1, this.map.size());
        ArrayList<Integer> expected = buildExpected(-2, 1);
        for (Integer i : map) {
            assertEquals(new Integer(0), i);
        }
    }

    @Test
    public void testRemoveSingleLeftRotation() {
        this.map.insert(3, "");
        this.map.insert(4, "");
        this.map.insert(2, "");
        this.map.insert(5, "");
        this.map.remove(2);
        assertFalse(this.map.has(2));
        assertEquals(3, this.map.size());
        ArrayList<Integer> expected = buildExpected(3, 5);
        int index = 0;
        for (Integer i : map) {
            assertEquals(expected.get(index++), i);
        }
    }

    @Test
    public void removeAnywhere() {
        this.populateMap();
        this.map.remove(SIZE - 2);
        assertEquals(SIZE - 1, this.map.size());
        this.map.remove(5);
        assertEquals(SIZE - 2, this.map.size());
        this.map.remove(1);
        assertEquals(SIZE - 3, this.map.size());
        this.map.remove(6);
        assertEquals(SIZE - 4, this.map.size());
    }

    @Test
    public void removeReturn() {
        this.map.insert(2, "2");
        this.map.insert(4, "4");
        this.map.insert(3, "3");
        this.map.insert(5, "");
        assertEquals("3", this.map.remove(3));
        assertEquals("2", this.map.remove(2));
        assertEquals("4", this.map.remove(4));
        assertFalse(this.map.has(3));
        assertFalse(this.map.has(2));
        assertFalse(this.map.has(4));
    }

    @Test
    public void bigData() {
        for (int i = 0; i < 10000; i++) {
            this.map.insert(i, "");
            ArrayList<Integer> expected = buildExpected(0, i);
            int index = 0;
            for (Integer j : map) {
                assertEquals(expected.get(index++), j);
            }
        }

        for (int i = 10000-1; i > 0; i--) {
            this.map.remove(i);
            ArrayList<Integer> expected = buildExpected(0, i);
            int index = 0;
            for (Integer j : map) {
                assertEquals(expected.get(index++), j);
            }
        }
    }
}
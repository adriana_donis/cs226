/**
* Adriana Donis Noriega.
* adonisn1@jhu.edu
* HW8
*/

/**
* Implements abstract createMap from MapTestBase for TreapMap
*/

public class TreapMapTest extends MapTestBase {
    
    @Override
    protected Map<Integer, String> createMap() {
        return new TreapMap<Integer, String>();
    }
}
import org.junit.Before;
import org.junit.Test;
import java.util.Iterator;
import java.util.Arrays;
import java.util.ArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;


public class AvlTest extends MapTestBase {
    @Override
    protected Map<Integer, String> createMap() {
        return new Avl<Integer, String>();
    }
}
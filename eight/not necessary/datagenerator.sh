python makedata.py 1000 1000000 0 > noRep1k.txt
python makedata.py 1000 1000000 50 > mixed1k.txt
python makedata.py 1000 1000000 0 > sorted1k.txt
sort -n noRep1k.txt > sorted1k.txt

python makedata.py 10000 1000000 0 > noRep10k.txt
python makedata.py 10000 1000000 50 > mixed10k.txt
python makedata.py 10000 1000000 0 > sorted10k.txt
sort -n noRep10k.txt > sorted10k.txt

python makedata.py 100000 1000000 0 > noRep100k.txt
python makedata.py 100000 1000000 50 > mixed100k.txt
python makedata.py 100000 1000000 0 > sorted100k.txt
sort -n noRep100k.txt > sorted100k.txt

/*
Adriana Donis Noriega
adonisn1@jhu.edu
HW3
*/

/** BubbleSort is a sorting algorithm that goes through an array
and compares one element with the next one, if the next one is smaller than the
element, it will swap the values. Its worst case complexity is O(n^2).

I used http://www.java-examples.com/java-bubble-sort-example as a reference.
@param <T> Element type
*/
public final class BubbleSort<T extends Comparable<T>>
    implements SortingAlgorithm<T> {

    // Helper to make code more readable.
    private boolean less(T a, T b) {
        return a.compareTo(b) < 0;
    }

    // Helper to make code more readable.
    private void swap(Array<T> a, int i, int j) {
        T t = a.get(i);
        a.put(i, a.get(j));
        a.put(j, t);
    }

    @Override
    public void sort(Array<T> array) {
        for (int i = 0; i < array.length(); i++) {
            for (int  j = 1; j < array.length() - i; j++) {
                if (this.less(array.get(j), array.get(j - 1))) {
                    this.swap(array, j, j - 1);
                }
            }
        }
    }

    @Override
    public String name() {
        return "Bubble Sort";
    }
}
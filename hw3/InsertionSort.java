/*
Adriana Donis Noriega
adonisn1@jhu.edu
HW3
*/

/** Insertion sort is a sorting algorithm that separates an array in two
parts, sorted and not sorted. The first value is always sorted. Then it
goes to the next and checks if it is bigger than the previous one it leaves
it there, but if it is smaller, it will swap it and repeat this process
until it puts it in the right place. Just like BubbleSort, it has a complexity
of O(n^2).

I used
https://en.wikibooks.org/wiki/Algorithm_Implementation/Sorting/Insertion_sort
as a reference.

@param <T> Element type
*/
public final class InsertionSort<T extends Comparable<T>>
    implements SortingAlgorithm<T> {

    // Helper to make code more readable.
    private boolean less(T a, T b) {
        return a.compareTo(b) < 0;
    }

    @Override
    public void sort(Array<T> array) {
        int ordered = 0;
        for (int i = 1; i < array.length(); i++) {
            T current = array.get(i);
            int j = i;
            while (j > 0 && this.less(current, array.get(j - 1))) {
                array.put(j, array.get(j - 1));
                j--;
            }
            array.put(j, current);
        }
    }

    @Override
    public String name() {
        return "Insertion Sort";
    }
}
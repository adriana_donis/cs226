/*
Adriana Donis Noriega
adonisn1@jhu.edu
HW3
*/

/** Stat-able keeps track of how many read (get and length) and write operations
have been performed. It extends SimpleArray, so get, put, and length work
like they do in SimpleArray, but now the counters are affected.

@param <T> Element type
*/
public class StatableArray<T> extends SimpleArray<T> implements Statable {

    private int read;
    private int write;

    /** Creates a new Statable array.
    @param n length of array
    @param t default value to store in each slot */
    public StatableArray(int n, T t) {
        super(n, t);
    }

    @Override
    public T get(int i) throws IndexException {
        T t = super.get(i);
        this.read++;
        return t;
    }

    @Override
    public void put(int i, T t) throws IndexException {
        super.put(i, t);
        this.write++;
    }

    @Override
    public int length() {
        this.read++;
        return super.length();
    }

    @Override
    public void resetStatistics() {
        this.read = 0;
        this.write = 0;
    }

    @Override
    public int numberOfReads() {
        return this.read;
    }

    @Override
    public int numberOfWrites() {
        return this.write;
    }
}
/*
Adriana Donis Noriega
adonisn1@jhu.edu
HW3
*/

import org.junit.Test;
import org.junit.BeforeClass;

import static org.junit.Assert.assertEquals;

public class StatableArrayTest {

    static StatableArray<Integer> statArray;
    int REPEATS = 5;

    @BeforeClass
    public static void setupArray() throws LengthException {
        statArray = new StatableArray<Integer>(15, 5);
    }

    @Test
    public void newNumberOfReads() {
        assertEquals(0, statArray.numberOfReads());
    }

    @Test
    public void newNumberOfWrites() {
        assertEquals(0, statArray.numberOfWrites());
    }

    @Test
    public void getNumberOfReads() {
        for (int i = 0; i < this.REPEATS; i++) {
            statArray.get(i);
        }
        assertEquals(this.REPEATS, statArray.numberOfReads());
    }

    @Test
    public void getNumberOfWrites() {
        for (int i = 0; i < this.REPEATS; i++) {
            statArray.put(i, 2);
        }
        assertEquals(this.REPEATS, statArray.numberOfWrites());
    }

    @Test
    public void reset() {
        statArray.resetStatistics();
        assertEquals(0, statArray.numberOfWrites());
        assertEquals(0, statArray.numberOfReads());
    }

    @Test
    public void getAndLengthNumberOfReads() {
        for (int i = 0; i < this.REPEATS; i++) {
            statArray.get(i);
        }
        statArray.length();
        assertEquals(this.REPEATS + 1, statArray.numberOfReads());
    }
}
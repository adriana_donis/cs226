/** Adriana Donis Noriega.
adonisn1@jhu.edu
HW4*/

/** ArrayDequeue implements the interface Dequeue.
* This implementation uses a SimpleArray. When the queue is full,
* the array is doubled. This class is ideal to use when values can be added and
* removed in the back or in the front.

* The values in the array aren't stored linearly all the
* time since they can be put in front or in the back, that is why,
* modulus operationsare used to be able to wrap around the stored values.
* Variables are used to keep
* track of the front and the back and print the queue in order.
* @param <T> Element type
*/
public class ArrayDequeue<T> implements Dequeue<T> {

    private Array<T> arrayDequeue;
    private int front;
    private int b;
    private int used;
    private int length = 1;

    /** Constructor. */
    public ArrayDequeue() {
        this.arrayDequeue = new SimpleArray<T>(1, null);
        this.used = 0;
    }

    /** Doubles the length of the array.*/
    private void grow() {
        Array<T> temporary = new SimpleArray<T>(this.length, null);
        int index = 0;
        for (int i = this.front; index < this.used; i =
            (i + 1 + this.length) % this.length) {
            temporary.put(index++, this.arrayDequeue.get(i));
        }
        if (this.length > 1) {
            this.b = this.length;
        }
        //this.b = this.length - 1;
        this.length *= 2;
        this.arrayDequeue = new SimpleArray<T>(this.length, null);
        index = 0;
        String s = "";
        for (T t : temporary) {
            this.arrayDequeue.put(index++, t);
            s += t + " ";
        }
        //System.out.println(s);
        this.front = 0;
    }

    @Override
    public boolean empty() {
        return this.used == 0;
    }

    @Override
    public int length() {
        return this.used;
    }

    @Override
    public T front() throws EmptyException {
        if (this.empty()) {
            throw new EmptyException();
        } else {
            return this.arrayDequeue.get(this.front);
        }
    }

    @Override
    public T back() throws EmptyException {
        if (this.empty()) {
            throw new EmptyException();
        } else {
            if (this.b == 0) {
                return this.arrayDequeue.get(this.length - 1);
            } else {
                return this.arrayDequeue.get(this.b - 1);
            }
        }
    }

    @Override
    public void insertFront(T t) {
        if (this.used == this.length) {
            this.grow();
        }
        this.front = (this.front - 1 + this.length) % this.length;
        this.used++;
        if (this.length == 1) {
            this.b = 1;
        }
        this.arrayDequeue.put(this.front, t);
    }

    @Override
    public void insertBack(T t) {
        if (this.used == this.length) {
            this.grow();
        }
        this.arrayDequeue.put(this.b, t);
        this.used++;
        if (this.length == 1) {
            this.b = 1;
        } else {
            this.b = (this.b + 1) % this.length;
        }
    }

    @Override
    public void removeFront() throws EmptyException {
        if (this.empty()) {
            throw new EmptyException();
        } else {
            this.used--;
            this.arrayDequeue.put(this.front, null);
            this.front = (this.front + 1 + this.length) % this.length;
        }
    }

    @Override
    public void removeBack() throws EmptyException {
        if (this.empty()) {
            throw new EmptyException();
        } else {
            this.used--;
            if (this.b == 0) {
                this.b = this.length - 1;
            } else {
                this.b = this.b - 1;
            }
        }
    }

    @Override
    public String toString() {
        if (this.empty()) {
            return "[]";
        }
        String s = "[";
        int next = this.front;
        int index = 0;
        int i;
        if (this.used == 1) {
            s = s + this.arrayDequeue.get(this.front) + "]";
        } else {
            for (i = this.front; index < this.used - 1; i =
                (i + 1 + this.length) % this.length) {
                s = s + this.arrayDequeue.get(i) + ", ";
                index++;
            }
            s = s + this.arrayDequeue.get(i) + "]";
        }
        return s;
    }
}

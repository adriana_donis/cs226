/** Adriana Donis Noriega
adonisn1@jhu.edu
HW4
*/

import java.util.Scanner;


/** Implementation of a basic RPN calculator that supports integer operands and
binary operations. The values are stored in a stack.
*/
public final class Calc {

    private static Stack<Integer> stack;

    // make checkstyle happy
    private Calc() {}

    /** Performs the operation between the last two elements of the stack.
    It checks that the stack has two values, if not, it catches an
    EmptyException.

    @param operation the operation that has to be performed.
    */
    public static void operate(String operation) {
        Integer first;
        Integer second;
        try {
            second = stack.top();
            stack.pop();
            try {
                first = stack.top();
                stack.pop();
                switch (operation) {
                    case "+":
                        stack.push(first + second);
                        break;
                    case "-":
                        stack.push(first - second);
                        break;
                    case "/":
                        divide(first, second);
                        break;
                    case "%":
                        stack.push(first % second);
                        break;
                    case "*":
                        stack.push(first * second);
                        break;
                    default:
                        break;
                }
            } catch (EmptyException ex) {
                System.err.println("?Not enough arguments.");
                stack.push(second);
            }
        } catch (EmptyException emptyEx) {
            System.err.println("?Not enough arguments.");
        }
    }

    /** Divides two integers. If the second Integer is 0, it catches an
    ArithmeticException.

    @param first nominator of division
    @param second denominator of division
    */
    public static void divide(Integer first, Integer second) {
        try {
            stack.push(first / second);
        } catch (ArithmeticException e) {
            System.err.println("?Can't divide by zero.");
            stack.push(first);
            stack.push(second);
        }
    }

    /** If the input is ".", then it prints and pops the last element.
    If the input is "?", then it prints the whole stack.
    If the input is "!" then it exits the program.
    Lastly, if the input is invalid (flaots or other strings), it prints to
    System.err a message saying the argument is invalid, and it ignores it.

    @param input command
    */
    public static void miscCommands(String input) {
        switch (input) {
            case "?":
                System.out.println(stack);
                break;
            case ".":
                try {
                    System.out.println(stack.top());
                    stack.pop();
                } catch (EmptyException ex) {
                    System.err.println("?Empty");
                }
                break;
            default:
                System.err.println("?Huh?");
                break;
        }
    }

    /**Initializes the stack. Then it reads the input from the user.
    If the input is a valid integer, it pushes it to the stack.
    If it is an operation (+ - % / *), then it calls the method operate().
    If it is another symbol, it calls miscCommands (miscellaneous commands)
    @param argv program arguments (ignored) */
    public static void main(String[] argv) {
        stack = new ArrayStack<Integer>();
        Scanner scan = new Scanner(System.in);
        String operations = "+-%*/";
        while (scan.hasNext()) {
            String input = scan.next();
            if ("!".equals(input)) {
                return;
            }
            try {
                int i = Integer.parseInt(input);
                stack.push(i);
            } catch (NumberFormatException e) {
                if (operations.contains(input)) {
                    operate(input);
                } else {
                    miscCommands(input);
                }
            }
        }
    }
}
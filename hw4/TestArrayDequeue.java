/** Adriana Donis Noriega
adonisn1@jhu.edu
HW4
*/

import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.Before;

import static org.junit.Assert.assertEquals;

public class TestArrayDequeue {

    static Dequeue<Integer> deq;
    static int REPEATS = 10;
    static int INTEGER = 5;
    static int INTEGER2 = -2;

    @Before
    public void setUpDequeue() throws LengthException {
        deq = new ArrayDequeue<Integer>();
    }


    @Test
    public void newLength() {
        assertEquals(0, deq.length());
    }

    @Test
    public void newEmpty() {
        assertEquals(true, deq.empty());
    }

    @Test(expected = EmptyException.class)
    public void newFront() throws EmptyException {
        deq.front();
    }

    @Test(expected = EmptyException.class)
    public void newBack() throws EmptyException {
        deq.back();
    }

    @Test(expected = EmptyException.class)
    public void newRemoveBack() throws EmptyException {
        deq.removeBack();
    }

    @Test(expected = EmptyException.class)
    public void newRemoveFront() throws EmptyException {
        deq.removeFront();
    }

    @Test
    public void newToString() {
        assertEquals("[]", deq.toString());
    }

    @Test
    public void newInsertFront() {
        deq.insertFront(1);
        assertEquals(false, deq.empty());
        assertEquals(1, deq.length());
        deq.insertFront(2);
        assertEquals(2, deq.length());
        assertEquals("[2, 1]", deq.toString());
        deq.insertFront(3);
        assertEquals(3, deq.length());
        deq.insertFront(4);
        deq.insertFront(5);
        assertEquals("[5, 4, 3, 2, 1]", deq.toString());
        for (int i = 0; i < REPEATS; i++) {
            deq.insertFront(INTEGER);
        }
        assertEquals(REPEATS + 5, deq.length());
    }

    @Test
    public void testInsertBack() {
        deq.insertBack(1);
        assertEquals(false, deq.empty());
        assertEquals(new Integer(1), deq.back());
        assertEquals("[1]", deq.toString());
        deq.insertBack(2);
        assertEquals("[1, 2]", deq.toString());
        assertEquals(new Integer(2), deq.back());
        deq.insertBack(3);
        assertEquals("[1, 2, 3]", deq.toString());
        assertEquals(new Integer(3), deq.back());
        deq.insertBack(4);
        assertEquals("[1, 2, 3, 4]", deq.toString());
        assertEquals(new Integer(4), deq.back());
        deq.insertBack(5);
        assertEquals("[1, 2, 3, 4, 5]", deq.toString());
        assertEquals(5, deq.length());
        assertEquals(new Integer(5), deq.back());
        for (int i = 0; i < 10; i++) {
            deq.insertBack(i);
        }
        assertEquals(new Integer(9), deq.back());
    }

    // Insert back before insert front
    @Test
    public void insertBackAndFront() {
        deq.insertFront(1);
        assertEquals(new Integer(1), deq.back());
        assertEquals(new Integer(1), deq.front());
        deq.insertBack(2);
        assertEquals(new Integer(2), deq.back());
        assertEquals(false, deq.empty());
        assertEquals("[1, 2]", deq.toString());
        deq.insertBack(3);
        deq.insertFront(4);
        assertEquals("[4, 1, 2, 3]", deq.toString());
        deq.insertFront(5);
        deq.insertFront(6);
        assertEquals(new Integer(6), deq.front());
        assertEquals(new Integer(3), deq.back());
        assertEquals("[6, 5, 4, 1, 2, 3]", deq.toString());
        deq.insertFront(7);
        assertEquals(new Integer(3), deq.back());
        assertEquals(new Integer(7), deq.front());
        assertEquals("[7, 6, 5, 4, 1, 2, 3]", deq.toString());
        deq.insertBack(8);
        assertEquals(new Integer(8), deq.back());
        assertEquals("[7, 6, 5, 4, 1, 2, 3, 8]", deq.toString());
    }

    @Test
    public void testRemoveFront() {
        deq.insertFront(2);
        deq.insertFront(1);
        deq.removeFront();
        assertEquals("[2]", deq.toString());
        deq.removeFront();
        assertEquals("[]", deq.toString());
        deq.insertBack(1);
        deq.removeFront();
        assertEquals("[]", deq.toString());
        assertEquals(0, deq.length());
        for (int i = 0; i < 5; i++) {
            deq.insertFront(5);
            deq.insertBack(2);
        }
        for (int i = 0; i < 5; i++) {
            deq.removeFront();
        }
        assertEquals("[2, 2, 2, 2, 2]", deq.toString());
    }

    @Test
    public void testRemoveBack() {
        deq.insertFront(2);
        deq.removeBack();
        assertEquals(0, deq.length());
        assertEquals("[]", deq.toString());
        deq.insertBack(5);
        deq.insertFront(-1);
        deq.removeBack();
        assertEquals(1, deq.length());
        assertEquals("[-1]", deq.toString());
        deq.removeBack();
        assertEquals(0, deq.length());
        for (int i = 0; i < 5; i++) {
            deq.insertFront(5);
            deq.insertBack(2);
        }
        for (int i = 0; i < 5; i++) {
            deq.removeBack();
        }
        assertEquals("[5, 5, 5, 5, 5]", deq.toString());
        for (int i = 0; i < 5; i++) {
            deq.removeFront();
        }
        assertEquals("[]", deq.toString());
    }

    @Test
    public void testRemoveFrontRemoveBack() {
        int j = 6;
        for (int i = 5; i  > 0; i--) {
            deq.insertFront(i);
            deq.insertBack(j++);
        }
        assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]", deq.toString());
        deq.removeBack();
        assertEquals(new Integer(9), deq.back());
        deq.removeFront();
        assertEquals(new Integer(2), deq.front());
        deq.removeBack();
        assertEquals(new Integer(8), deq.back());
        assertEquals(new Integer(2), deq.front());
        while (!deq.empty()) {
            deq.removeBack();
        }
        assertEquals(0, deq.length());
    }

    @Test
    public void testAll() {
        int j = 6;
        for (int i = 5; i  > 0; i--) {
            deq.insertFront(i);
            deq.insertBack(j++);
        }
        //deq.removeBack();
        deq.insertFront(deq.front());
        assertEquals("[1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]", deq.toString());
        deq.insertBack(deq.back());
        assertEquals("[1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10]", deq.toString());
        deq.removeBack();
        deq.insertBack(10);
        assertEquals("[1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10]", deq.toString());
        deq.insertFront(deq.back());
        assertEquals("[10, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10]",
            deq.toString());
    }

    public void testBig() {
        for (int i = 0; i < 100; i++) {
            deq.insertFront(i);
            deq.insertBack(i * -1);
        }
        assertEquals(100 * 2, deq.length());
        while (!deq.empty()) {
            deq.removeFront();
        }
        assertEquals("[]", deq.toString());
        assertEquals(0, deq.length());
    }
}
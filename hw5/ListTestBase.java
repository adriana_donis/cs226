/**Adriana Donis Noriega
adonisn1@jhu.edu
HW5*/

import org.junit.Before;
import org.junit.Test;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Testing implementations of the List interface.
 *
 * The tests defined here apply to all implementations of the List
 * interface. However, they cannot be run directly as we don't know
 * which implementation to test or how to create an instance of it.
 *
 * The solution is to define a "template method" called createList()
 * that subclasses of this test override. The NodeListTest.java class,
 * for example, creates a suitable NodeList instance to be tested.
 *
 * (We could use a JUnit feature called "parameterized tests" to do
 * the same thing, however that feature is a bit more complex to use
 * than we would like.)
 *
 * Note that we (somewhat arbitrarily) choose to test lists of strings.
 * We could have gone for lists of integers or lists of whatever, but
 * strings seem convenient in any case: You can pick strings in such a
 * way as to make your test cases more readable.
 */
public abstract class ListTestBase {
    private List<String> list;
    private List<String> otherList;

    protected abstract List<String> createList();

    @Before
    public void setupListTests() {
        this.list = this.createList();
        this.otherList = this.createList();
    }

    @Test
    public void newListEmpty() {
        assertTrue(this.list.empty());
        assertEquals(0, this.list.length());
        assertEquals("[]", this.list.toString());

        int c = 0;
        for (String s: this.list) {
            c++;
        }
        assertEquals(0, c);
    }

    @Test(expected = EmptyException.class)
    public void newListNoFront() {
        Position<String> p = this.list.front();
    }

    @Test(expected = EmptyException.class)
    public void newListNoBack() {
        Position<String> p = this.list.back();
    }

    @Test
    public void insertFrontWorks() {
        this.list.insertFront("One");
        this.list.insertFront("Two");
        this.list.insertFront("Three");

        assertFalse(this.list.empty());
        assertEquals(3, this.list.length());
        assertEquals("[Three, Two, One]", this.list.toString());

        int c = 0;
        for (String s: this.list) {
            c++;
        }
        assertEquals(3, c);
    }

    @Test
    public void insertBackWorks() {
        this.list.insertBack("One");
        this.list.insertBack("Two");
        this.list.insertBack("Three");

        assertFalse(this.list.empty());
        assertEquals(3, this.list.length());
        assertEquals("[One, Two, Three]", this.list.toString());

        int c = 0;
        for (String s: this.list) {
            c++;
        }
        assertEquals(3, c);
    }

    @Test
    public void insertFrontBackConsistent() {
        Position<String> f = this.list.insertFront("Front");
        assertEquals("Front", f.get());
        Position<String> b = this.list.insertBack("Back");
        assertEquals("Back", b.get());

        assertNotEquals(f, b);

        assertTrue(this.list.first(f));
        assertTrue(this.list.last(b));

        Position<String> x;

        x = this.list.front();
        assertEquals(f, x);

        x = this.list.back();
        assertEquals(b, x);
    }

    @Test
    public void removeFrontWorks() {
        this.list.insertFront("One");
        this.list.insertFront("Two");
        this.list.insertFront("Three");
        this.list.removeFront();
        this.list.removeFront();

        assertFalse(this.list.empty());
        assertEquals(1, this.list.length());
        assertEquals("[One]", this.list.toString());

        int c = 0;
        for (String s: this.list) {
            c++;
        }
        assertEquals(1, c);
    }

    @Test
    public void removeBackWorks() {
        this.list.insertFront("One");
        this.list.insertFront("Two");
        this.list.insertFront("Three");
        this.list.removeBack();
        this.list.removeBack();

        assertFalse(this.list.empty());
        assertEquals(1, this.list.length());
        assertEquals("[Three]", this.list.toString());

        int c = 0;
        for (String s: this.list) {
            c++;
        }
        assertEquals(1, c);
    }

    // TODO You need to add *many* more test cases here, ideally before you
    // even start working on Sentinelthis.list!

    @Test
    public void getFrontWorks() {
        this.list.insertFront("One");
        assertEquals("One", this.list.front().get());
        this.list.insertBack("Two");
        assertEquals("One", this.list.front().get());
    }

    @Test
    public void getBackWorks() {
        this.list.insertFront("One");
        assertEquals("One", this.list.back().get());
        this.list.insertBack("Two");
        assertEquals("Two", this.list.back().get());
    }

    @Test
    public void insertBeforeWorks() {
        Position<String> p = this.list.insertFront("One");
        Position<String> q = this.list.insertBefore(p, "Before One");
        assertEquals("Before One", this.list.front().get());
        assertEquals(2, this.list.length());
        this.list.insertBefore(p, "Between both");
        assertEquals("Before One", this.list.front().get());
        assertEquals("[Before One, Between both, One]", this.list.toString());
    }

    @Test
    public void insertAfterWorks() {
        Position<String> p = this.list.insertFront("One");
        Position<String> q = this.list.insertAfter(p, "After One");
        assertEquals("One", this.list.front().get());
        assertEquals("After One", this.list.back().get());
        assertEquals(2, this.list.length());
        this.list.insertAfter(p, "Between both");
        assertEquals("One", this.list.front().get());
        assertEquals("After One", this.list.back().get());
        assertEquals("[One, Between both, After One]", this.list.toString());
    }

    @Test
    public void insertBeforeAfterWork() {
        Position<String> p = this.list.insertFront("One");
        Position<String> q = this.list.insertAfter(p, "Two");
        Position<String> r = this.list.insertBefore(q, "Between 1 and 2");
        assertEquals("[One, Between 1 and 2, Two]", this.list.toString());
    }

    @Test
    public void removeWork() {
        Position<String> p = this.list.insertFront("One");
        Position<String> q = this.list.insertAfter(p, "Two");
        assertEquals(2, this.list.length());
        this.list.remove(p);
        assertEquals(1, this.list.length());
        assertEquals(q, this.list.front());
        assertEquals(q, this.list.back());
        assertEquals("[Two]", this.list.toString());
    }

    @Test(expected = EmptyException.class)
    public void newListRemoveBack() {
        this.list.removeBack();
    }

    @Test(expected = EmptyException.class)
    public void newListRemoveFront() {
        this.list.removeFront();
    }

    @Test(expected = PositionException.class)
    public void nextLastValue() {
        Position<String> p = this.list.insertFront("One");
        this.list.next(p);
    }

    @Test(expected = PositionException.class)
    public void previousFirstValue() {
        Position<String> p = this.list.insertFront("One");
        this.list.previous(p);
    }

    @Test(expected = PositionException.class)
    public void nextInvalidPosition() {
        this.list.insertFront("One");
        Position<String> otherP = this.otherList.insertFront("Other");
        this.list.next(otherP);
    }

    @Test(expected = PositionException.class)
    public void previousInvalidPosition() {
        this.list.insertFront("One");
        Position<String> otherP = this.otherList.insertFront("Other");
        this.list.previous(otherP);
    }

    @Test(expected = PositionException.class)
    public void insertBeforeInvalidPosition() {
        Position<String> otherP = this.otherList.insertFront("Other");
        this.list.insertBefore(otherP, "This List");
    }

    @Test(expected = PositionException.class)
    public void insertAfterInvalidPosition() {
        Position<String> otherP = this.otherList.insertFront("Other");
        this.list.insertAfter(otherP, "This List");
    }

    @Test(expected = PositionException.class)
    public void insertBeforeRemovedPosition() {
        Position<String> p = this.list.insertFront("Will remove");
        this.list.remove(p);
        this.list.insertBefore(p, "Can't insert");
    }

    @Test(expected = PositionException.class)
    public void insertAfterRemovedPosition() {
        Position<String> p = this.list.insertFront("Will remove");
        this.list.remove(p);
        this.list.insertAfter(p, "Can't insert");
    }

    @Test(expected = PositionException.class)
    public void removeRemoved() {
        Position<String> p = this.list.insertFront("Will remove");
        this.list.remove(p);
        this.list.remove(p);
    }

    @Test(expected = PositionException.class)
    public void removeRemovedBack() {
        Position<String> p = this.list.insertFront("One");
        Position<String> q = this.list.insertBack("Two");
        Position<String> r = this.list.insertBack("Three");
        this.list.removeBack();
        this.list.remove(r);
    }

    @Test(expected = PositionException.class)
    public void removeRemovedFront() {
        Position<String> p = this.list.insertFront("One");
        Position<String> q = this.list.insertBack("Two");
        Position<String> r = this.list.insertBack("Three");
        this.list.removeFront();
        this.list.remove(p);
    }

    @Test(expected = PositionException.class)
    public void firstInvalidPosition() {
        Position<String> p = this.otherList.insertFront("In other");
        this.list.first(p);
    }

    @Test(expected = PositionException.class)
    public void firstRemovedPosition() {
        Position<String> p = this.list.insertFront("Will remove");
        this.list.remove(p);
        this.list.first(p);
    }

    @Test(expected = PositionException.class)
    public void lastInvalidPosition() {
        Position<String> p = this.otherList.insertFront("In other");
        this.list.last(p);
    }

    @Test(expected = PositionException.class)
    public void lastRemovedPosition() {
        Position<String> p = this.list.insertFront("Will remove");
        this.list.remove(p);
        this.list.last(p);
    }

    @Test
    public void newListLength() {
        assertEquals(0, this.list.length());
    }

    @Test
    public void firstWorks() {
        Position<String> p = this.list.insertFront("First");
        Position<String> q = this.list.insertBack("Last");
        assertTrue(this.list.first(p));
        assertFalse(this.list.first(q));
    }

    @Test
    public void lastWorks() {
        Position<String> p = this.list.insertFront("First");
        Position<String> q = this.list.insertBack("Last");
        assertTrue(this.list.last(q));
        assertFalse(this.list.last(p));
    }

    @Test
    public void nextWorks() {
        Position<String> p = this.list.insertFront("One");
        Position<String> q = this.list.insertBack("Two");
        Position<String> r = this.list.insertBack("Three");
        assertEquals(q, this.list.next(p));
        assertEquals(r, this.list.next(q));
    }

    @Test
    public void previousWorks() {
        Position<String> p = this.list.insertFront("One");
        Position<String> q = this.list.insertBack("Two");
        Position<String> r = this.list.insertBack("Three");
        assertEquals(q, this.list.previous(r));
        assertEquals(p, this.list.previous(q));
        this.list.remove(q);
        assertEquals(p, this.list.previous(r));
    }

    @Test(expected = PositionException.class)
    public void nextOfLast() {
        Position<String> p = this.list.insertFront("One");
        Position<String> q = this.list.insertBack("Two");
        Position<String> r = this.list.insertBack("Three");
        this.list.next(r);
    }

    @Test(expected = PositionException.class)
    public void previousOfFirst() {
        Position<String> p = this.list.insertFront("One");
        Position<String> q = this.list.insertBack("Two");
        Position<String> r = this.list.insertBack("Three");
        this.list.previous(p);
    }

    @Test
    public void backwardIterator() {
        Position<String> p = this.list.insertFront("One");
        Position<String> q = this.list.insertBack("Two");
        Position<String> r = this.list.insertBack("Three");
        Iterator<String> iterator = this.list.backward();
        String[] array = new String[3];
        int i = 0;
        while (iterator.hasNext()) {
            array[i++] = iterator.next();
        }
        String[] expected = {"Three", "Two", "One"};
        for (int j = 0; j < 3; j++) {
            assertEquals(expected[j], array[j]);
        }
    }

    @Test
    public void forwardIterator() {
        Position<String> p = this.list.insertFront("One");
        Position<String> q = this.list.insertBack("Two");
        Position<String> r = this.list.insertBack("Three");
        Iterator<String> iterator = this.list.forward();
        String[] array = new String[3];
        int i = 0;
        while (iterator.hasNext()) {
            array[i++] = iterator.next();
        }
        String[] expected = {"One", "Two", "Three"};
        for (int j = 0; j < 3; j++) {
            assertEquals(expected[j], array[j]);
        }
    }

    @Test
    public void removingAll() {
        for (int i = 0; i < 50; i++) {
            this.list.insertFront("F");
            this.list.insertBack("B");
        }
        for (int i = 0; i < 50; i++) {
            this.list.removeBack();
            this.list.removeFront();
        }
        assertTrue(this.list.empty());
        assertEquals(0, this.list.length());
    }
}
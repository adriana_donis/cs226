/**Adriana Donis Noriega
adonisn1@jhu.edu
HW5*/

import java.util.Iterator;

/**
 * A generic position-based linked list.
 *
 * Here we use the "standard" representation of a doubly-linked list with
 * head (front, first) and tail (back, last) pointers. Even the empty list
 * contains two nodes (sentinels (head and tail). This simplifies
 * the implementation
 * and avoids the uses of null and checking for specific cases.
 *
 * @param <T> Type for elements.
 */
public class SentinelList<T> implements List<T> {

    private static final class Node<T> implements Position<T> {
        // The usual doubly-linked list stuff.
        Node<T> next;
        Node<T> prev;
        T data;

        // List that created this node, used to validate positions.
        List<T> owner;

        @Override
        public T get() {
            return this.data;
        }

        @Override
        public void put(T t) {
            this.data = t;
        }
    }

    private final class ListIterator implements Iterator<T> {
        Node<T> current;
        boolean forward;

        ListIterator(boolean f) {
            this.forward = f;
            if (this.forward) {
                this.current = SentinelList.this.head.next;
            } else {
                this.current = SentinelList.this.tail.prev;
            }
        }

        @Override
        public T next() {
            T t = this.current.get();
            if (this.forward) {
                this.current = this.current.next;
            } else {
                this.current = this.current.prev;
            }
            return t;
        }

        @Override
        public boolean hasNext() {
            if (this.forward) {
                return this.current != SentinelList.this.tail;
            } else {
                return this.current != SentinelList.this.head;
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private Node<T> head;
    private Node<T> tail;
    private int length;

    /** Create a list with two nodes in it. */
    public SentinelList() {

        this.head = new Node<T>();
        this.tail = new Node<T>();
        this.head.next = this.tail;
        this.tail.prev = this.head;
    }

    // Convert a position back into a node. Guards against null positions,
    // positions from other data structures, and positions that belong to
    // other NodeList objects.
    private Node<T> convert(Position<T> p) throws PositionException {
        try {
            Node<T> n = (Node<T>) p;
            if (n.owner != this) {
                throw new PositionException();
            }
            return n;
        } catch (NullPointerException | ClassCastException e) {
            throw new PositionException();
        }
    }

    @Override
    public boolean empty() {
        return this.length == 0;
    }

    @Override
    public int length() {
        return this.length;
    }

    @Override
    public boolean first(Position<T> p) throws PositionException {
        Node<T> n = this.convert(p);
        return this.head.next == n;
    }

    @Override
    public boolean last(Position<T> p) throws PositionException {
        Node<T> n = this.convert(p);
        return this.tail.prev == n;
    }

    @Override
    public Position<T> front() throws EmptyException {
        if (this.empty()) {
            throw new EmptyException();
        }
        return this.head.next;
    }

    @Override
    public Position<T> back() throws EmptyException {
        if (this.empty()) {
            throw new EmptyException();
        }
        return this.tail.prev;
    }

    /**
    * This is a helper method for when a node
    * needs to be inserted after one. (two cases: insert front of the
    * list and insert after specific node)
    * @param current new node needs to be added after this
    * @param t value of node
    * @return position of new node added */
    private Position<T> insertFrontAfter(Node<T> current, T t) {
        Node<T> n = new Node<T>();
        n.data = t;
        n.owner = this;
        n.prev = current;
        n.next = current.next; // N -> CN
        current.next.prev = n; // N <-> CN
        current.next = n; // C -> N <-> CN
        n.prev = current; // C <-< N <-> CN
        this.length++;
        return n;
    }

    @Override
    public Position<T> insertFront(T t) {
        return this.insertFrontAfter(this.head, t);
    }

    @Override
    public Position<T> insertAfter(Position<T> p, T t)
    throws PositionException {
        Node<T> current = this.convert(p);
        return this.insertFrontAfter(current, t);
    }

    /**
    * This is a helper method for when a node
    * needs to be inserted before one. (two cases: insert
    * back of the list and insert before specific node)
    * @param current new node needs to be added before this
    * @param t value of node
    * @return position of new node added */
    public Position<T> insertBackBefore(Node<T> current, T t) {
        Node<T> n = new Node<T>();
        n.data = t;
        n.owner = this;
        n.prev = current.prev;
        current.prev.next = n;
        current.prev = n;
        n.next = current;
        this.length++;
        return n;
    }

    @Override
    public Position<T> insertBack(T t) {
        return this.insertBackBefore(this.tail, t);
    }

    @Override
    public Position<T> insertBefore(Position<T> p, T t)
    throws PositionException {
        Node<T> current = this.convert(p);
        return this.insertBackBefore(current, t);
    }

    @Override
    public void removeFront() throws EmptyException {
        if (this.empty()) {
            throw new EmptyException();
        }
        this.remove(this.head.next);
    }

    @Override
    public void removeBack() throws EmptyException {
        if (this.empty()) {
            throw new EmptyException();
        }
        this.remove(this.tail.prev);
    }

    @Override
    public Position<T> next(Position<T> p) throws PositionException {
        if (this.last(p)) {
            throw new PositionException();
        }
        return this.convert(p).next;
    }

    @Override
    public Position<T> previous(Position<T> p) throws PositionException {
        if (this.first(p)) {
            throw new PositionException();
        }
        return this.convert(p).prev;
    }

    @Override
    public void remove(Position<T> p) throws PositionException {
        Node<T> n = this.convert(p);
        n.prev.next = n.next;
        n.next.prev = n.prev;
        n.owner = null;
        this.length--;
    }

    @Override
    public Iterator<T> forward() {
        return new ListIterator(true);
    }

    @Override
    public Iterator<T> backward() {
        return new ListIterator(false);
    }

    @Override
    public Iterator<T> iterator() {
        return this.forward();
    }

    @Override
    public String toString() {
        String s = "[";
        for (Node<T> p = this.head.next; p != this.tail; p = p.next) {
            s += p.data;
            if (p.next != this.tail) {
                s += ", ";
            }
        }
        s += "]";
        return s;
    }
}
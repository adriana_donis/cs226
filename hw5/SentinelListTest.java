/**Adriana Donis Noriega
adonisn1@jhu.edu
HW5*/

/** Instantiate the NodeList to test. */
public class SentinelListTest extends ListTestBase {
    @Override
    protected List<String> createList() {
        return new SentinelList<String>();
    }
}

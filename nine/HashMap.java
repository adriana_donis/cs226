/**
* Adriana Donis Noriega
* adonisn1@jhu.edu
* HW9
*/

import java.util.ArrayList;
import java.util.Iterator;

/**
* HashTable that implements the Map interface
* using separate chaining with Treaps.
*
* @param <K> Type for keys.
* @param <V> Type for values.
*/
public class HashMap<K extends Comparable<? super K>, V>
    implements Map<K, V> {

    private ArrayList<Entry> array;
    private int arraysize  = 2000;
    private int secondmod = 13;
    private double loadfactor = 0.40;
    private int size;

    /** Constructor. Initializes the Array */
    public HashMap() {
        this.array = new ArrayList<>();
        for (int i = 0; i < this.arraysize; i++) {
            this.array.add(null);
        }
    }

    private class Entry {
        K key;
        V value;

        Entry(K k, V v) {
            this.key = k;
            this.value = v;
        }

        Entry() { }

        public V getV() {
            return this.value;
        }

        public K getK() {
            return this.key;
        }

        public void setV(V v) {
            this.value = v;
        }

        public void setK(K k) {
            this.key = k;
        }
    }

    @Override
    public void insert(K k, V v) {
        if (k == null) {
            throw new IllegalArgumentException("Can't insert for null");
        }
        this.reHash();
        int hash = Math.abs(k.hashCode());
        int index = hash % this.arraysize;
        if (this.array.get(index) == null
            || this.array.get(index).getK() == null) {
            if (this.array.get(index) != null) {
                this.array.get(index).setK(k);
                this.array.get(index).setV(v);
            }
            this.array.set(index, new Entry(k, v));
            this.size++;
            return;
        } else {
            int hash2 = hash % this.secondmod + 1;
            while (this.array.get(index) != null) {
                if (this.array.get(index).getK().equals(k)) {
                    throw new IllegalArgumentException("Already in map");
                }
                index = (index + hash2) % this.arraysize;
            }
            this.array.set(index, new Entry(k, v));
            this.size++;
            return;
        }
    }

    @Override
    public boolean has(K k) {
        if (k == null) {
            return false;
        }
        return this.find(k) >= 0;
    }

    @Override
    public V get(K k) {
        if (k == null) {
            throw new IllegalArgumentException("Can't get null");
        }
        int index = this.find(k);
        if (index < 0) {
            throw new IllegalArgumentException("Can't find key");
        } else {
            return this.array.get(index).getV();
        }
    }

    @Override
    public void put(K k, V v) {
        if (k == null) {
            throw new IllegalArgumentException("Key can't be null");
        }
        if (!this.has(k)) {
            throw new IllegalArgumentException("Key dones't exist");
        }
        int index = this.find(k);
        this.array.get(index).setV(v);
        return;
    }

    private int find(K k) {
        int hash = Math.abs(k.hashCode());
        int index = hash % this.arraysize;
        int hash2 = hash % this.secondmod + 1;
        while (this.array.get(index) != null) {
            K k2 = this.array.get(index).getK();
            if (k.equals(k2)) {
                return index;
            } else {
                index = (index + hash2) % this.arraysize;
            }
        }
        return -1;
    }

    @Override
    public V remove(K k) {
        if (k == null) {
            throw new IllegalArgumentException("Can't remove null");
        }
        int index = this.find(k);
        if (index < 0) {
            throw new IllegalArgumentException("cannot find key " + k);
        }
        V v = this.array.get(index).getV();
        this.array.get(index).setK(null);
        this.size--;
        return v;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public String toString() {
        String s = "{";
        int counter = 0;
        for (Entry e : this.array) {
            if (e != null) {
                s += e.getK() + ":" + e.getV();
                if (counter++ < this.size - 1) {
                    s += ", ";
                }
            }

        }
        s += "}";
        return s;
    }

    @Override
    public Iterator<K> iterator() {
        ArrayList<K> keys = new ArrayList<>();
        for (Entry e : this.array) {
            if (e != null && e.getK() != null) {
                keys.add(e.getK());
            }
        }
        return keys.iterator();
    }


    // If there are more entries than the load factor
    // to avoid infinite loop.
    private void reHash() {
        if (this.size > this.arraysize * this.loadfactor) {
            this.loadfactor += 0.05;
            if (this.loadfactor >= 0.8) {
                this.loadfactor = 0.7;
            }
            ArrayList<Entry> copy = this.array;
            this.array = new ArrayList<>();
            this.arraysize *= 3;
            for (int i = 0; i < this.arraysize; i++) {
                this.array.add(null);
            }
            for (Entry e : copy) {
                if (e != null) {
                    this.insert(e.getK(), e.getV());
                }
            }
        }
        return;
    }
}
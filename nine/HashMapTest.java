/**
* Adriana Donis Noriega.
* adonisn1@jhu.edu
* HW8
*/

/**
* Implements abstract createMap from MapTestBase for TreapMap.
*/

public class HashMapTest extends MapTestBase {

    @Override
    protected Map<Integer, String> createMap() {
        return new HashMapNew<Integer, String>();
    }
}
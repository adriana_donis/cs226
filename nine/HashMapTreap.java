/**
* Adriana Donis Noriega
* adonisn1@jhu.edu
* HW9
*/

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.List;


/**
* HashTable that implements the Map interface
* using separate chaining with Treaps.
*
* @param <K> Type for keys.
* @param <V> Type for values.
*/
public class HashMapTreap<K extends Comparable<? super K>, V>
    implements Map<K, V> {

    private ArrayList<Treap> buckets;
    private int bound = 25;
    private int size;
    private int entries;

    /** Constructor. Initializes the buckets. */
    public HashMapTreap() {
        this.buckets = new ArrayList<Treap>();
        for (int i = 0; i < this.bound; i++) {
            Treap treap = new Treap();
            this.buckets.add(treap);
        }
    }

    private class Node {
        Node left;
        Node right;
        double priority;
        K key;
        V value;

        // Constructor to make node creation easier to read.
        Node(K k, V v, double p) {
            // left and right default to null
            this.key = k;
            this.value = v;
            this.priority = p;
        }

    }

    private class Treap {
        private Node root;
        private int size;
        private StringBuilder stringBuilder;
        private Random random = new Random(1);

        public int size() {
            return this.size;
        }

        public Node find(K k) {
            if (k == null) {
                throw new IllegalArgumentException("cannot handle null key");
            }
            Node n = this.root;
            while (n != null) {
                int cmp = k.compareTo(n.key);
                if (cmp < 0) {
                    n = n.left;
                } else if (cmp > 0) {
                    n = n.right;
                } else {
                    return n;
                }
            }
            return null;
        }

        public boolean has(K k) {
            if (k == null) {
                return false;
            }
            return this.find(k) != null;
        }

        private Node findForSure(K k) {
            Node n = this.find(k);
            if (n == null) {
                throw new IllegalArgumentException("cannot find key " + k);
            }
            return n;
        }

        private Node insert(Node n, K k, V v, double priority) {
            if (n == null) {
                return new Node(k, v, priority);
            }
            int cmp = k.compareTo(n.key);
            if (cmp < 0) {
                n.left = this.insert(n.left, k, v, priority);
                if (this.priorityIsLess(n.left, n)) {
                    n = this.swapRight(n);
                }
            } else if (cmp > 0) {
                n.right = this.insert(n.right, k, v, priority);
                if (this.priorityIsLess(n.right, n)) {
                    n = this.swapLeft(n);
                }
            } else {
                throw new IllegalArgumentException("duplicate key " + k);
            }
            return n;
        }

        public void insert(K k, V v) {
            double priority = this.random.nextDouble();
            if (k == null) {
                throw new IllegalArgumentException("cannot handle null key");
            }
            this.root = this.insert(this.root, k, v, priority);
            this.size += 1;
        }

        private Node swapLeft(Node oldParent) {
            Node newP = oldParent.right;
            oldParent.right = newP.left;
            newP.left = oldParent;
            return newP;
        }

        private Node swapRight(Node oldParent) {
            Node newP = oldParent.left;
            oldParent.left = newP.right;
            newP.right = oldParent;
            return newP;
        }

        private boolean priorityIsLess(Node child, Node parent) {
            if (child.priority < parent.priority) {
                return true;
            }
            return false;
        }

        private Node max(Node n) {
            while (n.right != null) {
                n = n.right;
            }
            return n;
        }

        private Node remove(Node n, K k) {
            if (n == null) {
                throw new IllegalArgumentException("cannot find key " + k);
            }

            int cmp = k.compareTo(n.key);
            if (cmp < 0) {
                n.left = this.remove(n.left, k);
            } else if (cmp > 0) {
                n.right = this.remove(n.right, k);
            } else {
                n = this.remove(n);
            }

            return n;
        }

        private Node remove(Node n) {
            // 0 and 1 child
            if (n.left == null) {
                return n.right;
            }
            if (n.right == null) {
                return n.left;
            }

            // 2 children
            Node max = this.max(n.left);
            n.key = max.key;
            n.value = max.value;
            n.left = this.remove(n.left, max.key);
            return n;
        }

        public V remove(K k) {
            Node n = this.find(k);
            if (n == null) {
                throw new IllegalArgumentException("cannot find key");
            }
            V v = n.value;
            this.root = this.remove(this.root, k);
            this.size -= 1;
            return v;
        }

        // Recursively add keys from subtree rooted at given node
        // into the given list.
        private void iteratorHelper(Node n, List<K> keys) {
            if (n == null) {
                return;
            }
            this.iteratorHelper(n.left, keys);
            keys.add(n.key);
            this.iteratorHelper(n.right, keys);
        }

        private ArrayList<K> arrayKeys() {
            ArrayList<K> keys = new ArrayList<K>();
            this.iteratorHelper(this.root, keys);
            return keys;
        }

        // If we don't have a StringBuilder yet, make one;
        // otherwise just reset it back to a clean slate.
        private void setupStringBuilder() {
            if (this.stringBuilder == null) {
                this.stringBuilder = new StringBuilder();
            } else {
                this.stringBuilder.setLength(0);
            }
        }

        // Recursively append string representations of keys and
        // values from subtree rooted at given node.
        private void toStringHelper(Node n, StringBuilder s) {
            if (n == null) {
                return;
            }
            this.toStringHelper(n.left, s);
            s.append(n.key);
            s.append(": ");
            s.append(n.value);
            s.append(", ");
            this.toStringHelper(n.right, s);
        }

        public String toString() {
            this.setupStringBuilder();

            this.toStringHelper(this.root, this.stringBuilder);

            int length = this.stringBuilder.length();
            if (length > 1) {
                // If anything was appended at all, get rid of
                // the last ", " the toStringHelper put in.
                this.stringBuilder.setLength(length - 2);
            }

            return this.stringBuilder.toString();
        }


        public void put(K k, V v) {
            Node n = this.findForSure(k);
            n.value = v;
        }

        public V get(K k) {
            Node n = this.findForSure(k);
            return n.value;
        }
    }

    @Override
    public void insert(K k, V v) {
        if (k == null) {
            throw new IllegalArgumentException("Can't insert null");
        }
        int index = this.hash(k);
        this.buckets.get(index).insert(k, v);
        this.size++;
    }

    private int valueOfK(int s) {
        return s;
    }

    private int hash(Object o) {
        return Math.abs(o.hashCode()) % this.bound;
    }

    @Override
    public V remove(K k) {
        if (k == null) {
            throw new IllegalArgumentException("Can't remove null");
        }
        V v = this.buckets.get(this.hash(k)).remove(k);
        this.size--;
        return v;
    }

    @Override
    public void put(K k, V v) {
        if (k == null) {
            throw new IllegalArgumentException("illegal");
        }
        this.buckets.get(this.hash(k)).put(k, v);
    }

    @Override
    public V get(K k) {
        if (k == null) {
            throw new IllegalArgumentException("Can't get null");
        }
        V v = this.buckets.get(this.hash(k)).get(k);
        return v;
    }

    @Override
    public boolean has(K k) {
        if (k == null) {
            return false;
        }
        return this.buckets.get(this.hash(k)).has(k);
    }

    @Override
    public int size() {
        return this.size;
    }


    @Override
    public String toString() {
        String s = "{";
        int en = 0;
        s = this.buckets.get(5).toString();
        for (Treap t : this.buckets) {
            if (t.size() != 0) {
                s += t.toString();
                if (en <= this.entries) {
                    s += ", ";
                }
                en++;
            }
        }
        s += "}";
        return s;
    }

    @Override
    public Iterator<K> iterator() {
        ArrayList<K> keys = new ArrayList<>();
        for (Treap t : this.buckets) {
            for (K k : t.arrayKeys()) {
                keys.add(k);
            }
        }
        return keys.iterator();
    }

}
/**
* Adriana Donis Noriega
* adonisn1@jhu.edu
* HW9
*/

import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Iterator;

public class HashMapTreaps<K extends Comparable<? super K>, V>
    implements OrderedMap<K, V> {

    private int SIZE = 15;
    private int place = 0;
	private int size;
	private ArrayList<BinarySearchTreeMap<K, V>> buckets;
	public int entries;


	public HashMapTreaps() { 
		buckets = new ArrayList<BinarySearchTreeMap<K,V>>(SIZE);
		for (int i = 0; i < SIZE; i++) {
			BinarySearchTreeMap<K, V> treap = new BinarySearchTreeMap<>();
			buckets.add(treap);
		}
	}

	@Override
	public void insert(K k, V v) { 
		//int index = func.hash();
		//int x = valueOfK(k);
		int index = this.hash(k);
		try {
			buckets.get(index).insert(k, v);
		} catch (IllegalArgumentException e) {
			//already existed
		}
		size++;
	}

	private int valueOfK(String s) {
		int hash = 7;
		for (int i = 0; i < s.length(); i++) {
    		hash = hash*31 + s.charAt(i);
		}
		return hash;
	}

	private int valueOfK(int s) {
		return s;
	}

	private int hash(Object o) {
		return Math.abs(o.hashCode()) % SIZE;
	}

	@Override
	public V remove(K k) {
		int index = this.hash(k);
		V v = buckets.get(index).remove(k);
		return v;
	}

	@Override
	public void put(K k, V v) {
		if (k == null) {
			throw new IllegalArgumentException("illegal");
		}
		int index = this.hash(k);
		buckets.get(index).put(k, v);
	}

	@Override
	public V get(K k) {
		int index = this.hash(k);
		V v =buckets.get(index).get(k);
		return v;
	}

	@Override
	public boolean has(K k) {
		int index = this.hash(k);
		return buckets.get(index).has(k);
	}

	@Override
	public int size() { 
		return this.size;
	}


	@Override
	public String toString() {
		String s = "";
		for (Map<K,V> t : buckets) {
			s += t.toString();
			s += "\n\n";
		}
		return s;
	}
	//private void iteratorHelper(Node n, ArrayList<K> keys) { }
    
    @Override
    public Iterator<K> iterator() { 
    	ArrayList<K> keys = new ArrayList<>();
    	for (Map<K, V> t : buckets) {
    		for (K k : t) {
    			keys.add(k);
    		}
    	}
    	return keys.iterator();
    }

}

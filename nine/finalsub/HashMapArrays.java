/**
* Adriana Donis Noriega
* adonisn1@jhu.edu
* HW9
*/

import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.List;

public class HashMapArrays<K extends Comparable<? super K>, V>
    implements Map<K, V> {

    private int SIZE = 100;
    private int place = 0;
	private int size;
	private ArrayList<ArrayList<Entry>> buckets;
	private HashFunction func = UniversalHashes.prime(SIZE);
	//private HashFunction thirdhash = UniversalHashes.power(SIZE);
	public int entries;


	private class Entry {
        K key;
        V value;

        Entry(K k, V v) {
            this.key = k;
            this.value = v;
        }

        Entry() { }

        public V getV() {
        	return value;
        }

        public K getK() {
        	return key;
        }
        public void setV(V v) {
        	this.value = v;
        }

        public void setK(K k) {
        	this.key = k;
        }
    }

	public HashMapArrays() { 
		buckets = new ArrayList<ArrayList<Entry>>();
		for (int i = 0; i < SIZE; i++) {
			ArrayList<Entry> array = new ArrayList<Entry>();
			for (int j = 0; j < SIZE; j++) {
				Entry e = new Entry();
				array.add(e);
			}
			buckets.add(array);
		}
	}

	@Override
	public void insert(K k, V v) { 
		//int index = func.hash();
		//int x = valueOfK(k);
		int index = this.hash(k);
		try {
			int index2 = Math.abs(func.hash(this.hashNoMod(k))) % SIZE;
			System.out.println("putting " + k + " " + index + " " + index2);
			Entry e = new Entry(k, v);
			while (buckets.get(index).get(index2).getK() != null) {
				index2++;
			}
			buckets.get(index).add(index2, e);
			//System.out.print(buckets.get(index).get(index2).getK());
		} catch (IllegalArgumentException e) {
			//already existed
		}
		entries++;
	}

	private int valueOfK(String s) {
		int hash = 7;
		for (int i = 0; i < s.length(); i++) {
    		hash = hash*31 + s.charAt(i);
		}
		return hash;
	}


	private int valueOfK(int s) {
		return s;
	}

	private int hashNoMod(Object o) {
		return o.hashCode();
	}

	private int hash(Object o) {
		return Math.abs(o.hashCode()) % SIZE;
	}

	@Override
	public V remove(K k) {
		int index = this.hash(k);
		int index2 = Math.abs(func.hash(this.hashNoMod(k))) % SIZE;
		while (!buckets.get(index).get(index2).getK().equals(k)) {
			index2++;
		}
		V v = buckets.get(index).get(index2).getV();
		return v;
	}

	@Override
	public void put(K k, V v) {
		int index = this.hash(k);
		int index2 = Math.abs(func.hash(this.hashNoMod(k))) % SIZE;
		buckets.get(index).get(index2).setV(v);
	}

	@Override
	public V get(K k) {
		int index = this.hash(k);
		int index2 = Math.abs(func.hash(this.hashNoMod(k))) % SIZE;
		while (!buckets.get(index).get(index2).getK().equals(k)) {
			index2++;
		}
		V v = buckets.get(index).get(index2).getV();;
		return v;
	}

	@Override
	public boolean has(K k) {
		int index = this.hash(k);
		int index2 = Math.abs(func.hash(this.hashNoMod(k))) % SIZE;
		//System.out.println("looking for " + k + " " + index + " " + index2);
		K k2 = buckets.get(index).get(index2).getK()
		while (!k2.equals(k)) {
			index2++;
		}
		K inspot = buckets.get(index).get(index2).getK();
		//System.out.println("inspot : " + inspot);
		if (inspot != null && inspot.equals(k)) {
			//System.out.println("equals");
			return true;
		}
		return false;
		//return buckets.get(index).get(index2).getK().equals(k);
	}

	@Override
	public int size() { 
		return this.size;
	}


	@Override
	public String toString() {
		String s = "";
		for (ArrayList<Entry> t : buckets) {
			for (Entry e : t) {
				s += e.getK() + " ";
			}
			//s += t.toString();
			s += "\n\n";
		}
		return s;
	}
	//private void iteratorHelper(Node n, ArrayList<K> keys) { }

    private void iteratorHelper(ArrayList<Entry> entries, List<K> keys) {
        for (Entry e : entries) {
        	keys.add(e.key);
        }
    }

    @Override
    public Iterator<K> iterator() {
        List<K> keys = new ArrayList<K>();
        for (ArrayList<Entry> entries : this.buckets) {
        	this.iteratorHelper(entries, keys);
        }
        return keys.iterator();
    }

}

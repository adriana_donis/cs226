/**
* @author Adriana Donis Noriega
* adonisn1@jhu.edu
* HW8
*/

import com.github.phf.jb.Bench;
import com.github.phf.jb.Bee;
import java.util.ArrayList;
import java.util.Collections;

import java.util.Random;

/**
 * Benchmark performance of HashMap.
 */
public final class HashMapBench {
    private static final int SIZE = 10000;
    private static final Random RAND = new Random();

    private HashMapBench() {}

    // Insert consecutive strings into the map.
    private static void insertLinear(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.insert(Integer.toString(i), i);
        }
    }

    // Insert a number of "random" Integers into the given map.
    private static void insertRandom(
        Map<String, Integer> m, ArrayList<String> r) {
        for (int i = 0; i < SIZE; i++) {
            m.insert(r.get(i), i);
        }
    }

    private static void lookupLinear(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            boolean x = m.has(Integer.toString(i));
        }
    }

    private static void lookupRandom(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            boolean x = m.has(Integer.toString(RAND.nextInt(SIZE)));
        }
    }

    private static void removeRandom(
        Map<String, Integer> m, ArrayList<String> r) {
        for (int i = 0; i < SIZE; i++) {
            m.remove(r.get(i));
        }
    }

    private static void removeLinear(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.remove(Integer.toString(i));
        }
    }

    private static void getLinear(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.get(Integer.toString(i));
        }
    }

    private static void getRandom(Map<String, Integer> m, 
        ArrayList<String> r) {
        for (String s : r) {
            m.get(s);
        }
    }

    private static void putLinear(Map<String, Integer> m) {
        for (int i = 0; i < SIZE; i++) {
            m.put(Integer.toString(i), i + 1);
        }
    }

    private static void putRandom(Map<String, Integer> m,
        ArrayList<String> r) {
        for (String s : r) {
            m.put(s, 1);
        }
    }

    private static ArrayList<String> createUniqueNumbers() {
        ArrayList<String> n = new ArrayList<>();
        for (int i = 0; i < SIZE; i++) {
            n.add(Integer.toString(i));
        }
        Collections.shuffle(n);
        return n;
    }

    @Bench
    public static void putLinearHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            insertLinear(m);
            b.start();
            putLinear(m);
        }
    }

    @Bench
    public static void putRandomHashMap(Bee b) {
        ArrayList<String> random = createUniqueNumbers();
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            insertLinear(m);
            b.start();
            putRandom(m, random);
        }
    }

    @Bench
    public static void getLinearHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            insertLinear(m);
            b.start();
            getLinear(m);
        }
    }

    @Bench
    public static void getRandomHashMap(Bee b) {
        ArrayList<String> random = createUniqueNumbers();
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            insertLinear(m);
            b.start();
            getRandom(m, random);
        }
    }

    @Bench
    public static void insertLinearHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            b.start();
            insertLinear(m);
        }
    }

    @Bench
    public static void insertRandomHashMap(Bee b) {
        ArrayList<String> random = createUniqueNumbers();
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            b.start();
            insertRandom(m, random);
        }
    }

    @Bench
    public static void lookupLinearHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            insertLinear(m);
            b.start();
            lookupLinear(m);
        }
    }

    @Bench
    public static void lookupRandomHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            insertLinear(m);
            b.start();
            lookupRandom(m);
        }
    }

    @Bench
    public static void removeLinearHashMap(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            insertLinear(m);
            b.start();
            removeLinear(m);
        }
    }

    @Bench
    public static void removeRandomHashMap(Bee b) {
        ArrayList<String> random = createUniqueNumbers();
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            insertLinear(m);
            b.start();
            removeRandom(m, random);
        }
    }

    @Bench
    public static void ranInsRanRemHashMap(Bee b) {
        ArrayList<String> randomI = createUniqueNumbers();
        ArrayList<String> randomR = createUniqueNumbers();
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            insertRandom(m, randomI);
            b.start();
            removeRandom(m, randomR);
        }
    }

    @Bench
    public static void ranInslinRemHashMap(Bee b) {
        ArrayList<String> randomI = createUniqueNumbers();
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Map<String, Integer> m = new HashMap<>();
            insertRandom(m, randomI);
            b.start();
            removeLinear(m);
        }
    }
}
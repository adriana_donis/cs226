/**
* @author Adriana Donis Noriega
* adonisn1@jhu.edu
* HW8
*/

import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
* Tests for Maps:
* newSize, newHas, insert, hasInsert, insertGet,
* insertPut (as putValue), insertSize, removeSize
* insertHas.
*
* Exceptions thrown:
* insert null
* insert already there
* remove null
* remove not in map
* put null
* put not in map
* get null
* get not in map
*/
public abstract class MapTestBase {

    int SIZE = 15;

    protected Map<Integer, String> map;

    abstract protected Map<Integer, String> createMap();

    @Before
    public void setupMap() {
        this.map = this.createMap();
    }

    @Test
    public void newSize() {
        assertEquals(0, map.size());
    }

    @Test
    public void newHas() {
        assertFalse(map.has(5));
    }

    @Test
    public void testInsert() {
        this.map.insert(10, "root");
        assertEquals(1, map.size());
    }

    @Test
    public void hasInsert() {
        this.map.insert(10, "root");
        assertTrue(this.map.has(10));
    }

    @Test
    public void insertGet() {
        this.map.insert(10, "root");
        assertEquals("root", this.map.get(10));
    }

    protected void populateMap() {
        for (int i = SIZE; i > 0; i--) {
            this.map.insert(i, Integer.toString(i));
        }
    }

    @Test
    public void sizeMap() {
        this.populateMap();
        assertEquals(SIZE, this.map.size());
    }

    @Test
    public void removeKey() {
        this.populateMap();
        assertEquals(Integer.toString(1), this.map.remove(1));
        assertEquals(this.SIZE - 1, this.map.size());
        assertFalse(this.map.has(1));
    }

    @Test
    public void putValue() {
        this.populateMap();
        this.map.put(1, "first");
        assertEquals("first", this.map.get(1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullPut() {
        this.map.put(null, "error");
    }

    @Test(expected = IllegalArgumentException.class)
    public void doesntExistPut() {
        this.populateMap();
        this.map.put(-5, "doesn't exist");
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertExisting() {
        this.populateMap();
        this.map.insert(1, "already there");
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertNull() {
        this.map.insert(null, "can't insert null");
    }

    @Test
    public void hasNull() {
        this.populateMap();
        assertFalse(this.map.has(null));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getNotInMap() {
        this.populateMap();
        this.map.get(SIZE + 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getNull() {
        this.populateMap();
        this.map.get(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeNull() {
        this.populateMap();
        this.map.remove(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeNotInMap() {
        this.populateMap();
        this.map.remove(SIZE + 1);
    }
    
    // Tests iterator as well
    @Test
    public void insertHas() {
        for (int i = 0; i < 1000; i++) {
            this.map.insert(i, Integer.toString(i));
        }
        ArrayList<Integer> expected = this.buildExpected(0, 999);
        int index = 0;
        for (Integer i : this.map) {
            assertTrue(this.map.has(expected.get(index++)));
        }

    }
    
    private ArrayList<Integer> buildExpected(int from, int to) {
        ArrayList<Integer> array = new ArrayList<Integer>();
        for (int i = from; i <= to; i++) {
            array.add(i);
        }
        return array;
    }


}
import com.github.phf.jb.Bench;
import com.github.phf.jb.Bee;

import java.util.Random;

/**
 * Compare performance of TransposeArraySet and MoveToFrontListSet.
 */
public final class AdaptiveSetsBench {
    private static final int SIZE = 100;
    private static final Random RAND = new Random();

    private AdaptiveSetsBench() {}

    private static void insertLinear(Set<String> s) {
        for (int i = 0; i < SIZE; i++) {
            s.insert(Integer.toString(i));
        }
    }

    // Insert a number of "random" strings into the given set.
    private static void insertRandom(Set<String> s) {
        for (int i = 0; i < SIZE; i++) {
            s.insert(Integer.toString(RAND.nextInt(SIZE * 4)));
        }
    }

    // Remove a number of "random" strings from the given set.
    private static void removeRandom(Set<String> s) {
        for (int i = 0; i < SIZE; i++) {
            s.remove(Integer.toString(RAND.nextInt(SIZE * 2)));
        }
    }

    // Lookup a number of "consecutive" strings in the given set.
    private static void lookupLinear(Set<String> s) {
        for (int i = 0; i < SIZE; i++) {
            boolean x = s.has(Integer.toString(i));
        }
    }

    // Lookup a number of "random" strings in the given set.
    private static void lookupRandom(Set<String> s) {
        for (int i = 0; i < SIZE; i++) {
            boolean x = s.has(Integer.toString(RAND.nextInt(SIZE)));
        }
    }

    @Bench
    public static void insertLinearATransposeArraySet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new TransposeArraySet<>();
            b.start();
            insertLinear(s);
        }
    }

    @Bench
    public static void insertLinearMoveToFrontListSet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new MoveToFrontListSet<>();
            b.start();
            insertLinear(s);
        }
    }

    @Bench
    public static void insertRandomATransposeArraySet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new TransposeArraySet<>();
            b.start();
            insertRandom(s);
        }
    }

    @Bench
    public static void insertRandomMoveToFrontListSet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new MoveToFrontListSet<>();
            b.start();
            insertRandom(s);
        }
    }

    @Bench
    public static void removeRandomATransposeArraySet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new TransposeArraySet<>();
            insertRandom(s);
            b.start();
            removeRandom(s);
        }
    }

    @Bench
    public static void removeRandomMoveToFrontListSet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new MoveToFrontListSet<>();
            insertRandom(s);
            b.start();
            removeRandom(s);
        }
    }

    @Bench
    public static void lookupLinearATransposeArraySet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new TransposeArraySet<>();
            insertLinear(s);
            b.start();
            lookupLinear(s);
        }
    }

    @Bench
    public static void lookupLinearMoveToFrontListSet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new MoveToFrontListSet<>();
            insertLinear(s);
            b.start();
            lookupLinear(s);
        }
    }

    @Bench
    public static void lookupRandomATransposeArraySet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new TransposeArraySet<>();
            insertLinear(s);
            b.start();
            lookupRandom(s);
        }
    }

    @Bench
    public static void lookupRandomMoveToFrontListSet(Bee b) {
        for (int n = 0; n < b.reps(); n++) {
            b.stop();
            Set<String> s = new MoveToFrontListSet<>();
            insertLinear(s);
            b.start();
            lookupRandom(s);
        }
    }

}
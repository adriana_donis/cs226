/**
* Adriana Donis Noriega
* adonisn1@jhu.edu
* HW7
*/

import org.junit.Before;
import org.junit.Test;
import java.util.Iterator;
import java.util.Arrays;
import java.util.ArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;


public class MoveToFrontListSetTest extends SetTestBase {
    @Override
    protected Set<Integer> createSet() {
        return new MoveToFrontListSet<Integer>();
    }
	
	@Test
    public void testInsert() {
        ArrayList<Integer> expected = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        for (int i = 5; i > 0; i--) {
            this.set.insert(i);
        }
        ArrayList<Integer> s = buildArray();
        assertEquals(expected, s);
    }

    @Test
    public void testRemove() {
        populateSet();
        this.set.remove(1);
        ArrayList<Integer> expected = new ArrayList<Integer>(Arrays.asList(2, 3, 4, 5));
        assertEquals(expected, buildArray());
        this.set.remove(2);
        expected.remove(0);
        assertEquals(expected, buildArray());
        this.set.remove(6); // doesn't exist
        assertEquals(expected, buildArray());
        this.set.remove(5);
        expected.remove(2);
        assertEquals(expected, buildArray());
    }

   	@Test
    public void moveFrontHas() {
        populateSet();
        this.set.has(5);
        ArrayList<Integer> expected
            = new ArrayList<Integer>(Arrays.asList(5, 1, 2, 3, 4));
        assertEquals(expected, buildArray());
        this.set.has(1);
        expected = new ArrayList<Integer>(Arrays.asList(1, 5, 2, 3, 4));
        assertEquals(expected, buildArray());
    }

    @Test
    public void moveFrontInsert() {
        populateSet();
        set.insert(5);
        ArrayList<Integer> expected
            = new ArrayList<Integer>(Arrays.asList(5, 1, 2, 3, 4));
        assertEquals(expected, buildArray());
        set.insert(1);
        expected = new ArrayList<Integer>(Arrays.asList(1, 5, 2, 3, 4));
        assertEquals(expected, buildArray());
    }
}
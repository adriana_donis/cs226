/**
* @author Adriana Donis Noriega
* adonisn1@jhu.edu
* HW7
*/

import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public abstract class SetTestBase {

    protected Set<Integer> set;

    protected abstract Set<Integer> createSet();

    @Before
    public void setUpSet() {
        this.set = this.createSet();
    }

    @Test
    public void newRemove() {
        this.set.remove(5);
        assertTrue(true); // didn't crash
    }

    protected void populateSet() {
        for (int i = 5; i > 0; i--) {
            this.set.insert(i);
        }
    }

    protected ArrayList<Integer> buildArray() {
        ArrayList<Integer> s = new ArrayList<Integer>();
        Iterator<Integer> it = this.set.iterator();
        while (it.hasNext()) {
            s.add(it.next());
        }
        return s;
    }

    @Test
    public void testHas() {
        populateSet();
        assertTrue(this.set.has(3));
        assertFalse(this.set.has(11));
    }

}
/**
* Adriana Donis Noriega
* adonisn1@jhu.edu
* HW7
*/

import java.util.Iterator;

/**
 * Set implemented using plain Java arrays.
 * @param <T> Element type.
 */
public class TransposeArraySet<T> implements Set<T> {
    private int used;
    private T[] data;

    private class SetIterator implements Iterator<T> {
        private int current;

        @Override
        public boolean hasNext() {
            return this.current < TransposeArraySet.this.used;
        }

        @Override
        public T next() {
            T t = TransposeArraySet.this.data[this.current];
            this.current += 1;
            return t;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * Make a set.
     */
    public TransposeArraySet() {
        this.data = (T[]) new Object[1];
    }

    /**
    * Returns a boolean if the set is full.
    * @return boolean true if it is full
    */
    private boolean full() {
        return this.used >= this.data.length;
    }

    /**
    * Makes the array twice the length. And copies to the new one.
    */
    private void grow() {
        T[] bigger = (T[]) new Object[2 * this.used];
        for (int i = 0; i < this.used; i++) {
            bigger[i] = this.data[i];
        }
        this.data = bigger;
    }

    /**
    * Goes through the array. If ithe value is there, it swaps it with the
    * previous value and then returns the index.
    * @param T value that we are looking for
    * @return int index where it is.
    */
    private int find(T t) {
        for (int i = 0; i < this.used; i++) {
            if (this.data[i].equals(t)) {
                if (i != 0) {
                    T temp = this.data[i];
                    this.data[i] = this.data[i - 1];
                    this.data[i - 1] = temp;
                    return i - 1; 
                } else {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public void insert(T t) {
        if (this.has(t)) {
            return;
        }
        if (this.full()) {
            this.grow();
        }
        this.data[this.used] = t;
        this.used += 1;
    }

    @Override
    public void remove(T t) {
        int p = this.find(t);
        if (p == -1) {
            return;
        }
        for (int i = p; i < this.used - 1; i++) {
            this.data[i] = this.data[i + 1];
        }
        this.used -= 1;
    }

    @Override
    public boolean has(T t) {
        return this.find(t) != -1;
    }

    @Override
    public Iterator<T> iterator() {
        return new SetIterator();
    }
}

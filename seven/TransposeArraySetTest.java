/**
* Adriana Donis Noriega
* adonisn1@jhu.edu
* HW7
*/

import org.junit.Before;
import org.junit.Test;
import java.util.Iterator;
import java.util.Arrays;
import java.util.ArrayList;
import static org.junit.Assert.assertEquals;

public class TransposeArraySetTest extends SetTestBase {
    
    @Override
    protected Set<Integer> createSet() {
        return new TransposeArraySet<Integer>();
    }

    @Test
    public void testInsert() {
        ArrayList<Integer> expected =
            new ArrayList<>(Arrays.asList(5, 4, 3, 2, 1));
        for (int i = 5; i > 0; i--) {
            this.set.insert(i);
        }
        ArrayList<Integer> s = buildArray();
        assertEquals(expected, s);
    }

    @Test
    public void testRemove() {
        populateSet();
        this.set.remove(1);
        ArrayList<Integer> expected =
            new ArrayList<Integer>(Arrays.asList(5, 4, 3, 2));
        assertEquals(expected, buildArray());
        this.set.remove(2);
        expected.remove(3);
        assertEquals(expected, buildArray());
        this.set.remove(6); // doesn't exist
        assertEquals(expected, buildArray());
        this.set.remove(5);
        expected.remove(0);
        assertEquals(expected, buildArray());
    }

   	@Test
    public void moveFrontHas() {
        populateSet();
        this.set.has(1);
        ArrayList<Integer> expected
            = new ArrayList<Integer>(Arrays.asList(5, 4, 3, 1, 2));
        assertEquals(expected, buildArray());
        this.set.has(1);
        expected = new ArrayList<Integer>(Arrays.asList(5, 4, 1, 3, 2));
        assertEquals(expected, buildArray());
        this.set.has(5);
        assertEquals(expected, buildArray());
    }

    @Test
    public void moveFrontInsert() {
        populateSet();
        set.insert(5);
        ArrayList<Integer> expected
            = new ArrayList<Integer>(Arrays.asList(5, 4, 3, 2, 1));
        assertEquals(expected, buildArray());
        set.insert(1);
        expected = new ArrayList<Integer>(Arrays.asList(5, 4, 3, 1, 2));
        assertEquals(expected, buildArray());
    }
}
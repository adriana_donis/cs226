python makedata.py 1000 1000000 90 > biased1k.txt
python makedata.py 1000 1000000 50 > mixed1k.txt
python makedata.py 1000 1000000 0 > random1k.txt

python makedata.py 10000 1000000 90 > biased10k.txt
python makedata.py 10000 1000000 50 > mixed10k.txt
python makedata.py 10000 1000000 0 > random10k.txt

python makedata.py 100000 1000000 90 > biased100k.txt
python makedata.py 100000 1000000 50 > mixed100k.txt
python makedata.py 100000 1000000 0 > random100k.txt


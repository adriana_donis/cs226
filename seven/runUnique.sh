javac Unique.java
echo 100,000 ----------------------
echo 90% 
./xtime java Unique < biased100k.txt
./xtime java Unique < biased100k.txt
./xtime java Unique < biased100k.txt

echo 50%
./xtime java Unique < mixed100k.txt
./xtime java Unique < mixed100k.txt
./xtime java Unique < mixed100k.txt

echo 0%

./xtime java Unique < random100k.txt
./xtime java Unique < random100k.txt
./xtime java Unique < random100k.txt

echo 10,000 ------------------------
 
echo 90%

./xtime java Unique < biased10k.txt
./xtime java Unique < biased10k.txt
./xtime java Unique < biased10k.txt

echo 50%
./xtime java Unique < mixed10k.txt
./xtime java Unique < mixed10k.txt
./xtime java Unique < mixed10k.txt

echo 0 %
./xtime java Unique < random10k.txt
./xtime java Unique < random10k.txt
./xtime java Unique < random10k.txt

echo 1,000 -----------------------
echo 90%

./xtime java Unique < biased1k.txt
./xtime java Unique < biased1k.txt
./xtime java Unique < biased1k.txt

echo 50%
./xtime java Unique < mixed1k.txt
./xtime java Unique < mixed1k.txt
./xtime java Unique < mixed1k.txt

echo 0 %
./xtime java Unique < random1k.txt
./xtime java Unique < random1k.txt
./xtime java Unique < random1k.txt





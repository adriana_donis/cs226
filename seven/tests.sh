#javac -Xlint:all Set.java
#javac -Xlint:all MoveToFrontListSet.java
#javac -Xlint:all TransposeArraySet.java

#javac -Xlint:all -classpath /usr/share/java/junit4.jar:. MoveToFrontListSetTest.java
#java -classpath /usr/share/java/junit4.jar:. org.junit.runner.JUnitCore MoveToFrontListSetTest

#javac -Xlint:all -classpath /usr/share/java/junit4.jar:. TransposeArraySetTest.java
#java -classpath /usr/share/java/junit4.jar:. org.junit.runner.JUnitCore TransposeArraySetTest

javac -Xlint:all PriorityQueue.java
javac -Xlint:all SortedArrayPriorityQueue.java
javac -Xlint:all BinaryHeapPriorityQueue.java

javac -Xlint:all -classpath /usr/share/java/junit4.jar:. SortedArrayPriorityQueueTest.java
javac -Xlint:all -classpath /usr/share/java/junit4.jar:. BinaryHeapPriorityQueueTest.java
java -classpath /usr/share/java/junit4.jar:. org.junit.runner.JUnitCore SortedArrayPriorityQueueTest
java -classpath /usr/share/java/junit4.jar:. org.junit.runner.JUnitCore BinaryHeapPriorityQueueTest
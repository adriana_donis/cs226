/**
* @author Adriana Donis Noriega
* adonisn1@jhu.edu
* HW6
*/

import java.util.Map;
import java.util.HashMap;
import java.util.Queue;
import java.util.LinkedList;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;


/**
*    Six Degrees of Angelina Jolie.
*
*    Some of the stuff I used in here is a little ahead of us. For
*    example we have not formally covered maps yet. But I am sure
*    you can nevertheless find your way around. :-)
*
*    Do NOT USE MAPS in your code for BFS below! Trust us, you can
*    make do just with the existing Graph interface and with Java's
*    Queue interface (since BFS needs a queue internally).
*/
public final class Angelina {
    // Graph holding movies and actors as vertices, relationships
    // as edges. All are simply strings.
    private static Graph<String, String> graph =
        new SparseGraph<String, String>();

    // Vertices for the actor we're trying to connect to Angelina
    // Jolie and for Angelina herself.
    private static Vertex<String> actor;
    private static Vertex<String> jolie;

    // Shut up checkstyle.
    private Angelina() {}

    // Read input file and turn it into a Graph.
    //
    // There's one line for each movie, with the fields separated
    // by "/". The first field is the movie, the remaining fields
    // are actors.
    //
    // Generates a bipartite graph in which BOTH movies and actors
    // are vertices. A graph in which all vertices are actors and
    // movies are edges would be HORRIBLE instead (why?).
    //
    // This function also sets up the "actor" and "jolie" globals
    // that will be used to direct the breadth-first-search.
    private static void readInput(String filename, String who)
        throws FileNotFoundException, IOException {
        // keep track of all vertices created so far by name
        Map<String, Vertex<String>> vertices = new HashMap<>();

        // how we read the input
        BufferedReader reader = new BufferedReader(new FileReader(
            new File(filename)));
        String line;

        while ((line = reader.readLine()) != null) {
            String[] data = line.split("/");

            // find or create vertex for the movie
            Vertex<String> m = vertices.get(data[0]);
            if (m == null) {
                m = graph.insert(data[0]);
                vertices.put(data[0], m);
            }

            for (int i = 1; i < data.length; i++) {
                // find or create vertex for the actor
                Vertex<String> a = vertices.get(data[i]);
                if (a == null) {
                    a = graph.insert(data[i]);
                    vertices.put(data[i], a);
                }

                // double-check for special actors
                if (a.get().equals("Jolie, Angelina")) {
                    jolie = a;
                }
                if (a.get().equals(who)) {
                    actor = a;
                }

                // create two edges, from and to the movie
                graph.insert(m, a, "features");
                graph.insert(a, m, "acts in");
                //System.out.println(m.get());
            }
        }
    }

    // Perform a breadth-first search (BFS) starting from Angelina Jolie
    // and stopping when (a) the graph is exhausted or (b) we found the
    // actor we're looking for. Then print the path from the actor back
    // to Angelina and exit the program. Since we're using BFS we can be
    // sure that the resulting path is among the shortest ones.
    private static void solveJolie() {
        graph.clearLabels();
        Queue<Vertex<String>> q = new LinkedList<Vertex<String>>();
        // start at Angelina
        q.add(jolie);
        //Vertex<String> previous = null;
        while (!q.isEmpty()) {
            // take first element of queue out.
            Vertex<String> v = q.remove();
            // check if it is has reached the stop point (the other actor)
            if (v.get().equals(actor.get())) {
                // print the actor
                System.out.println(v.get());
                // go backwards through the visited Vertexes using the labels
                // until it gets to jolie.
                while (v.get() != jolie.get() && graph.label(v) != null) {
                    // next thing to print will be the vertex visited before
                    v = (Vertex<String>) graph.label(v);
                    System.out.println(v.get());
                }
                System.exit(0);
            } else {
                // add all the vertexes pointed to by the outgoing edges of v
                for (Edge<String> ed : graph.outgoing(v)) {
                    // get where it edge goes to
                    Vertex<String> to = graph.to(ed);
                    if (graph.label(to) == null) { // if it does have children
                        // add to the queue
                        q.add(to);
                        // change the label to the vertex wher it comes from
                        graph.label(to, v);
                    }
                }
            }
        }
        // If it didn't find it.
        System.out.println(actor.get()
            + " is lame and doesn't have a Jolie number");
        System.exit(0);
    }

    /**
    *    Main method.
    *    @param args Command line arguments.
    *    @throws FileNotFoundException If database file cannot be opened.
    *    @throws IOException If database file cannot be read properly.
    */
    public static void main(String[] args)
        throws FileNotFoundException, IOException {
        // read the input, initialize globals
        readInput(args[0], args[1]);

        // check that we could find both actors, quit if not
        if (actor == null) {
            System.out.printf("Error: Can't find %s in database.\n", args[1]);
            System.exit(1);
        }
        if (jolie == null) {
            System.out.printf("Error: Can't find Angelina in database.\n");
            System.exit(1);
        }

        // play "six degrees of Anglina Jolie" using breadth-first search
        solveJolie();
    }
}

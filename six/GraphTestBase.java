/**
* @author Adriana Donis Noriega
* adonisn1@jhu.edu
* HW6
*/

import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public abstract class GraphTestBase {

    protected Graph<String, Integer> graph;
    protected Graph<String, Integer> otherGraph;

    protected abstract Graph<String, Integer> createGraph();

    @Before
    public void setUpGraphTests() {
        this.graph = this.createGraph();
        this.otherGraph = this.createGraph();
    }

    @Test
    public void newClearLabels() {
        graph.clearLabels();
        assertTrue(true); // check that program didn't crash when clearing
    }

    @Test
    public void newIterableVertices() {
        Iterable<Vertex<String>> verts = this.graph.vertices();
        Iterator<Vertex<String>> vertIt = verts.iterator();
        String dataVertices = "";
        while (vertIt.hasNext()) {
            dataVertices += vertIt.next().get();
        }
        assertEquals("", dataVertices);
    }

    @Test
    public void edgeTo() {
        Vertex<String> v1 = this.graph.insert("A");
        Vertex<String> v2 = this.graph.insert("B");
        Edge<Integer> e1 = this.graph.insert(v1, v2, 1);
        assertEquals(v2, this.graph.to(e1));
    }

    @Test
    public void edgeFrom() {
        Vertex<String> v1 = this.graph.insert("A");
        Vertex<String> v2 = this.graph.insert("B");
        Edge<Integer> e1 = this.graph.insert(v1, v2, 1);
        assertEquals(v1, this.graph.from(e1));
    }

    @Test(expected = PositionException.class)
    public void insertEdgeOtherGraphSecond() {
        Vertex<String> v1 = this.graph.insert("A");
        Vertex<String> v2 = this.otherGraph.insert("B");
        Edge<Integer> e1 = this.graph.insert(v1, v2, 1);
    }

    @Test(expected = PositionException.class)
    public void insertEdgeOtherGraphFirst() {
        Vertex<String> v1 = this.otherGraph.insert("A");
        Vertex<String> v2 = this.graph.insert("B");
        Edge<Integer> e1 = this.graph.insert(v1, v2, 1);
    }

    @Test(expected = InsertionException.class)
    public void invalidSelfLoop() {
        Vertex<String> v1 = this.graph.insert("A");
        Edge<Integer> e1 = this.graph.insert(v1, v1, 1);
    }

    @Test(expected = InsertionException.class)
    public void edgeAlreadyExists() {
        Vertex<String> v1 = this.graph.insert("A");
        Vertex<String> v2 = this.graph.insert("B");
        Edge<Integer> e1 = this.graph.insert(v1, v2, 1);
        Edge<Integer> e2 = this.graph.insert(v1, v2, 1);
    }

    @Test(expected = RemovalException.class)
    public void removeTest() {
        Vertex<String> v1 = this.graph.insert("A");
        Vertex<String> v2 = this.graph.insert("B");
        Vertex<String> v3 = this.graph.insert("C");
        Edge<Integer> e1 = this.graph.insert(v1, v2, 1);
        Edge<Integer> e2 = this.graph.insert(v2, v3, 2);
        Edge<Integer> e3 = this.graph.insert(v3, v1, 3);
        this.graph.remove(v1);
    }

    @Test(expected = PositionException.class)
    public void removeInvalidPosition() {
        Vertex<String> v1 = this.graph.insert("A");
        Vertex<String> v2 = this.graph.insert("B");
        Vertex<String> v3 = this.otherGraph.insert("Invalid");
        Edge<Integer> e1 = this.graph.insert(v1, v2, 1);
        this.graph.remove(v3);
    }

    @Test
    public void successfulRemove() {
        Vertex<String> v1 = this.graph.insert("A");
        Vertex<String> v2 = this.graph.insert("B");
        Vertex<String> v3 = this.graph.insert("C");
        assertEquals("A", this.graph.remove(v1));
        assertEquals("B", this.graph.remove(v2));
        Iterable<Vertex<String>> vertices = this.graph.vertices();
        String vertData = "";
        for (Vertex<String> v : vertices) {
            vertData += v.get();
        }
        assertEquals("C", vertData);
    }

    @Test 
    public void successfulRemoveEdge() {
        Vertex<String> v1 = this.graph.insert("A");
        Vertex<String> v2 = this.graph.insert("B");
        Vertex<String> v3 = this.graph.insert("C");
        Edge<Integer> e1 = this.graph.insert(v1, v2, 1);
        Edge<Integer> e2 = this.graph.insert(v2, v3, 2);
        Edge<Integer> e3 = this.graph.insert(v3, v1, 3);

        assertEquals(new Integer(1), this.graph.remove(e1));
    }

    @Test(expected = PositionException.class)
    public void invalidRemoveEdge() {
        Vertex<String> v1 = this.otherGraph.insert("A");
        Vertex<String> v2 = this.otherGraph.insert("B");
        Edge<Integer> e1 = this.otherGraph.insert(v1, v2, 1);
        this.graph.remove(e1);
    }

    @Test
    public void successfulRemoveVertex() {
        Vertex<String> v1 = this.graph.insert("A");
        Vertex<String> v2 = this.graph.insert("B");
        Edge<Integer> e1 = this.graph.insert(v1, v2, 1);
        this.graph.remove(e1);
        this.graph.remove(v1);
        this.graph.remove(v2);
    }

    @Test(expected = PositionException.class)
    public void testInvalidPositionOutgoing() {
        Vertex<String> v1 = this.otherGraph.insert("A");
        this.graph.outgoing(v1);
    }

    @Test(expected = PositionException.class)
    public void testInvalidPositionIncoming() {
        Vertex<String> v1 = this.otherGraph.insert("A");
        this.graph.incoming(v1);
    }

    @Test
    public void testFrom() {
        Vertex<String> v1 = this.graph.insert("FROM");
        Vertex<String> v2 = this.graph.insert("TO");
        Edge<Integer> e1 = this.graph.insert(v1, v2, 1);
        assertEquals(v1, this.graph.from(e1));
    }

    @Test(expected = PositionException.class)
    public void invalidFrom() {
        Vertex<String> v1 = this.graph.insert("FROM");
        Vertex<String> v2 = this.graph.insert("TO");
        Edge<Integer> e1 = this.graph.insert(v1, v2, 1);
        this.otherGraph.from(e1);
    }

    @Test
    public void testTo() {
        Vertex<String> v1 = this.graph.insert("FROM");
        Vertex<String> v2 = this.graph.insert("TO");
        Edge<Integer> e1 = this.graph.insert(v1, v2, 1);
        assertEquals(v2, this.graph.to(e1));
    }

    @Test(expected = PositionException.class)
    public void invalidTo() {
        Vertex<String> v1 = this.graph.insert("FROM");
        Vertex<String> v2 = this.graph.insert("TO");
        Edge<Integer> e1 = this.graph.insert(v1, v2, 1);
        this.otherGraph.to(e1);
    }

    @Test
    public void labelVertex() {
        Vertex<String> v1 = this.graph.insert("FROM");
        this.graph.label(v1, "LABELED");
        Object labelObject = this.graph.label(v1);
        assertEquals("LABELED", (String) labelObject);
        Vertex<String> v2 = this.graph.insert("TO");
        assertEquals(null, this.graph.label(v2));
    }

    @Test(expected = PositionException.class)
    public void invalidLabelVertex() {
        Vertex<String> v1 = this.otherGraph.insert("FROM");
        this.graph.label(v1, "LABELED");
    }

    @Test
    public void labelEdge() {
        Vertex<String> v1 = this.graph.insert("FROM");
        Vertex<String> v2 = this.graph.insert("TO");
        Edge<Integer> e1 = this.graph.insert(v1, v2, 1);
        this.graph.label(e1, "LABELED");
        assertEquals("LABELED", (String) this.graph.label(e1));
    }

    @Test(expected = PositionException.class)
    public void invalidLabelEdge() {
        Vertex<String> v1 = this.graph.insert("FROM");
        Vertex<String> v2 = this.graph.insert("TO");
        Edge<Integer> e1 = this.graph.insert(v1, v2, 1);
        this.otherGraph.label(e1, "INVALID LABEL");
    }

    @Test(expected = PositionException.class)
    public void invalidLabelEdge2() {
        Vertex<String> v1 = this.graph.insert("FROM");
        Vertex<String> v2 = this.graph.insert("TO");
        Edge<Integer> e1 = this.graph.insert(v1, v2, 1);
        this.graph.label(e1, "LABEL");
        this.otherGraph.label(e1);
    }

    @Test
    public void clearLabels() {
        Vertex<String> v2 = this.graph.insert("B");
        int totalEdges = 50;
        for (int i = 0; i < totalEdges; i++) {
            Edge<Integer> e = this.graph.insert(this.graph.insert("A"), v2, i);
            this.graph.label(e, "LABEL");
        }
        Iterable<Vertex<String>> vertices = this.graph.vertices();
        for (Vertex<String> vertex : vertices) {
            this.graph.label(vertex, "LABEL");
        }
        this.graph.clearLabels();
        for (Vertex<String> vertex : vertices) {
            assertEquals(null, this.graph.label(vertex));
        }
        Iterable<Edge<Integer>> edges = this.graph.edges();
        for (Edge<Integer> e : edges) {
            assertEquals(null, this.graph.label(e));
        }
    }
}
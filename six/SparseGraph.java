/**
* @author Adriana Donis Noriega
* adonisn1
* HW6
*/

import java.util.ArrayList;

/**
 * Directed graphs.
 *
 * Vertices and edges carry *one* uniform type of
 * data.
 *
 * Instead of our customary Position interface we use Vertex and Edge
 * interfaces as positions. We can therefore overload method names to
 * keep down interface complexity and (more importantly) we get some
 * degree of static type safety: clients who confuse vertex and edge
 * positions will notice at compile-time (unless they try really hard
 * not to anyway).
 *
 * SparseGraph allows users to keep track of the vertices and edges
 * they have. The graph can be printed out by using System.out.print(name)
 * and it will output the correct format to use GraphViz and have a
 * graphical representation of it.
 *
 * Functions that return iterables, actually return a copy of the
 * Array to avoid the client from removing a node from the graph.
 *
 * Labels are used to traverse the list and mark the Vertexes, and Edges
 * that have already been visited.
 *
 * This implementation contains to inner classes. One for the vertices
 * (Vx) and one for the edges (Ed). A Vx stores data, the onwer (graph),
 * a label of type Object and to Arrays. One stores the incoming edges,
 * and the othe the outgoing ones. An Ed stores data, label, owner and
 * two Vx (from and to).
 *
 * The convert function is overloaded.
 * @param <V> Vertex element type.
 * @param <E> Edge element type.
 */
public class SparseGraph<V, E> implements Graph<V, E> {

    // Contains vertices, each vertex contains a list of Edges.
    ArrayList<Vertex<V>> verts;

    /**
    * Create an empty graph.
    */
    public SparseGraph() {
        this.verts = new ArrayList<Vertex<V>>();
    }

    private final class Vx<V> implements Vertex<V> {
        V data;
        Object label;
        Graph<V, E> owner; // to identify it belongs to that data structure
        ArrayList<Edge<E>> in = new ArrayList<Edge<E>>();
        ArrayList<Edge<E>> out = new ArrayList<Edge<E>>();

        @Override
        public V get() {
            return this.data;
        }

        @Override
        public void put(V v) {
            this.data = v;
        }
    }

    private final class Ed<E> implements Edge<E> {
        Vertex<V> from;
        Vertex<V> to;
        Object label;
        E data;
        Graph<V, E> owner;

        @Override
        public E get() {
            return this.data;
        }

        @Override
        public void put(E e) {
            this.data = e;
        }
    }

    /**
    * Convert a Edge back into a Ed. Guards against null Edge,
    * Edge from other data structures, and Edge that belong to
    * other Graph objects.
    * @param ed edge that needs to be converted
    * @return converted Ed to usable object
    */
    private Ed<E> convert(Edge<E> ed) throws PositionException {
        try {
            Ed<E> e = (Ed<E>) ed;
            if (e.owner != this) { // if it belongs to another graph
                throw new PositionException();
            }
            return e;
        } catch (NullPointerException | ClassCastException e) {
            throw new PositionException();
        }
    }

    /**
    * Convert a Vertex back into a Vx. Guards against null Vertex,
    * Vertex from other data structures, and Vertex that belong to
    * other Graph objects.
    * @param vx edge that needs to be converted
    * @return converted Vx to usable object
    */
    private Vx<V> convert(Vertex<V> vx) throws PositionException {
        try {
            Vx<V> v = (Vx<V>) vx;
            if (v.owner != this) {
                throw new PositionException();
            }
            return v;
        } catch (NullPointerException | ClassCastException e) {
            throw new PositionException();
        }
    }

    @Override
    public Vertex<V> insert(V v) {
        Vx<V> vertex = new Vx<V>();
        vertex.put(v); // assign v to data.
        vertex.owner = this;
        this.verts.add(vertex);
        return vertex;
    }

    @Override
    public Edge<E> insert(Vertex<V> from, Vertex<V> to, E e)
        throws PositionException, InsertionException {
        Vx<V> toV = this.convert(to);
        Vx<V> fromV = this.convert(from);
        if (toV == fromV) { // avoid self loop
            throw new InsertionException();
        }
        for (Edge<E> incoming : toV.in) {
            Ed<E> edge = this.convert(incoming);
            if (edge.to == toV && edge.from == fromV) {
                throw new InsertionException();
            }
        }
        Ed<E> ed = new Ed<E>();
        ed.put(e); // put data
        ed.to = to;
        ed.from = from;
        ed.owner = this;
        fromV.out.add(ed); // add to outgoing edges
        toV.in.add(ed); // add to incoming edges
        return ed;
    }

    @Override
    public V remove(Vertex<V> v) throws PositionException, RemovalException {
        Vx<V> vertex = this.convert(v);
        V element = vertex.data;
        if (vertex.out.isEmpty() && vertex.in.isEmpty()) {
            vertex.owner = null; // invalidate the vertex
            this.verts.remove(vertex); // remove from arraylist of vertices
        } else {
            throw new RemovalException();
        }
        return element;
    }

    @Override
    public E remove(Edge<E> e) throws PositionException {
        Ed<E> edge = this.convert(e);
        E data = edge.data;
        edge.owner = null; // invalidate edge
        Vx<V> to = this.convert(edge.to);
        to.in.remove(edge); // remove from arraylist of incoming edges
        Vx<V> from = this.convert(edge.from);
        // remove from arraylist of outgoing edges of from
        from.out.remove(edge);
        return data;
    }

    // returns a copy to avoid the client from messing up with the graph
    // if they try to remove
    @Override
    public Iterable<Vertex<V>> vertices() {
        ArrayList<Vertex<V>> copy = new ArrayList<Vertex<V>>(this.verts);
        return copy;
    }

    @Override
    public Iterable<Edge<E>> edges() {
        ArrayList<Edge<E>> edges = new ArrayList<Edge<E>>();
        for (Vertex<V> v : this.verts) {
            for (Edge<E> e : this.outgoing(v)) {
                edges.add(e);
            }
        }
        return edges;
    }

    @Override
    public Iterable<Edge<E>> outgoing(Vertex<V> v) throws PositionException {
        Vx<V> vertex = this.convert(v);
        ArrayList<Edge<E>> copy = new ArrayList<Edge<E>>(vertex.out);
        return copy;
    }

    @Override
    public Iterable<Edge<E>> incoming(Vertex<V> v) throws PositionException {
        Vx<V> vertex = this.convert(v);
        ArrayList<Edge<E>> copy = new ArrayList<Edge<E>>(vertex.in);
        return copy;
    }

    @Override
    public Vertex<V> from(Edge<E> e) throws PositionException {
        Ed<E> ed = this.convert(e);
        Vertex<V> vx = ed.from;
        return vx;
    }

    @Override
    public Vertex<V> to(Edge<E> e) throws PositionException {
        Ed<E> ed = this.convert(e);
        Vertex<V> vx = ed.to;
        return vx;
    }

    @Override
    public void label(Vertex<V> v, Object l) throws PositionException {
        Vx<V> vx = this.convert(v);
        vx.label = l;
        return;
    }

    @Override
    public void label(Edge<E> e, Object l) throws PositionException {
        Ed<E> ed = this.convert(e);
        ed.label = l;
        return;
    }

    @Override
    public Object label(Vertex<V> v) throws PositionException {
        Vx<V> vertex = this.convert(v);
        return vertex.label;
    }

    @Override
    public Object label(Edge<E> e) throws PositionException {
        Ed<E> ed = this.convert(e);
        return ed.label;
    }

    @Override
    public void clearLabels() {
        for (Edge<E> e : this.edges()) {
            Ed<E> ed = this.convert(e);
            ed.label = null;
        }
        for (Vertex<V> v : this.verts) {
            Vx<V> vx = this.convert(v);
            vx.label = null;
        }
        return;
    }

    @Override
    public String toString() {
        String format = "digraph {\n";
        for (Vertex<V> v : this.verts) {
            Vx<V> vertex = this.convert(v);
            format += "  \""  + vertex.data + "\";\n";
        }
        for (Edge<E> e : this.edges()) {
            Ed<E> edge = this.convert(e);
            format += "  \"" + this.convert(edge.from).data + "\"" + " -> "
                + "\"" + this.convert(edge.to).data + "\"" + " [label=\""
                + edge.data + "\"];\n";
        }

        format += "}";
        return format;
    }
}
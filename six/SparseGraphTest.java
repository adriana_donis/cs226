/**
* Adriana Donis Noriega
* adonisn1@jhu.edu
* HW6
*/

import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Iterator;
import static org.junit.Assert.assertEquals;

public class SparseGraphTest extends GraphTestBase {
    @Override
    protected Graph<String, Integer> createGraph() {
        return new SparseGraph<String, Integer>();
    }


    @Test
    public void newToString() {
        String output = graph.toString();
        assertEquals("digraph {\n}", output);
    }

    @Test
    public void toStringTest() {
        Vertex<String> v1 = graph.insert("A");
        Vertex<String> v2 = graph.insert("B");
        Edge<Integer> e1 = graph.insert(v1, v2, 1);
        String v1string = "\"" + v1.get() + "\"";
        String v2string = "\"" + v2.get() + "\"";
        String e1string = "\"" + e1.get() + "\"";
        String expected = "digraph {\n  \"A\";\n  \"B\";\n  \"A\" -> \"B\" [label=\"1\"];\n}";  
        assertEquals(expected, graph.toString());
    }

    @Test
    public void unchangedGraphAfterRemoval() {
        Vertex<String> v1 = graph.insert("A");
        Vertex<String> v2 = graph.insert("B");
        Vertex<String> v3 = graph.insert("C");
        Iterable<Vertex<String>> vertices = graph.vertices();
        Iterator<Vertex<String>> vertIt = vertices.iterator();
        String original = graph.toString();
        while (vertIt.hasNext()) {;
            vertIt.next();
            vertIt.remove();
        }
        String afterRemove = graph.toString();
        assertEquals(original, afterRemove);
    }

    @Test
    public void newIterableEdges() {
        Iterable<Edge<Integer>> edges = this.graph.edges();
        Iterator<Edge<Integer>> edgesIt = edges.iterator();
        String dataEdges = "";
        while (edgesIt.hasNext()) {
            dataEdges += edgesIt.next().get();
        }
        assertEquals("", dataEdges);
    }
    
    @Test
    public void testVerticesIterable() {
        int totalVertices = 50;
        for (int i = 0; i < totalVertices; i++) {
            this.graph.insert("A");
        }
        Iterable<Vertex<String>> iterable = this.graph.vertices();
        int counter = 0;
        for (Vertex<String> v : iterable) {
            counter++;
        }
        assertEquals(totalVertices, counter);
    }

    @Test
    public void testEdgesIterable() {
        int totalEdges = 50;
        for (int i = 0; i < totalEdges; i++) {
            this.graph.insert(graph.insert("A"), this.graph.insert("B"), 1);
        }
        Iterable<Edge<Integer>> iterable = this.graph.edges();
        int counter = 0;
        for (Edge<Integer> v : iterable) {
            counter++;
        }
        assertEquals(totalEdges, counter);
    }

    @Test
    public void testOutgoingIterable() {
        Vertex<String> v1 = this.graph.insert("A");
        int totalEdges = 50;
        for (int i = 0; i < totalEdges; i++) {
            this.graph.insert(v1, this.graph.insert("B"), 1);
        }
        Iterable<Edge<Integer>> outgoing = this.graph.outgoing(v1);
        int counter = 0;
        for (Edge<Integer> edge : outgoing) {
            counter++;
            assertEquals(new Integer(1), edge.get());
        }
        assertEquals(totalEdges, counter);
    }

    @Test
    public void testIncomingIterable() {
        Vertex<String> v1 = this.graph.insert("A");
        int totalEdges = 50;
        for (int i = 0; i < totalEdges; i++) {
            this.graph.insert(v1, this.graph.insert("B"), 1);
        }
        Iterable<Edge<Integer>> incoming = this.graph.incoming(v1);
        int counter = 0;
        for (Edge<Integer> edge : incoming) {
            counter++;
            assertEquals(new Integer(1), edge.get());
        }
        assertEquals(0, counter);
    }

    @Test
    public void testIncomingIterable2() {
        Vertex<String> v2 = this.graph.insert("B");
        int totalEdges = 50;
        for (int i = 0; i < totalEdges; i++) {
            this.graph.insert(graph.insert("A"), v2, i);
        }
        Iterable<Edge<Integer>> incoming = this.graph.incoming(v2);
        int counter = 0;
        for (Edge<Integer> edge : incoming) {
            counter++;
        }
        assertEquals(totalEdges, counter);
    }


    @Test
    public void unchangedGraphRemovalVertexAndEdges() {
        Vertex<String> v2 = graph.insert("B");
        int totalEdges = 50;
        for (int i = 0; i < totalEdges; i++) {
            Edge<Integer> e = graph.insert(graph.insert("A"), v2, i);
            graph.label(e, "LABEL");
        }
        Iterable<Vertex<String>> vertices = graph.vertices();
        for (Vertex<String> vertex : vertices) {
            graph.label(vertex, "LABEL");
        }
        // original graph
        String original = graph.toString();
        // trying to remove all edges
        Iterable<Edge<Integer>> edges = graph.edges();
        Iterator<Edge<Integer>> edIt = edges.iterator();
        while (edIt.hasNext()) {
            edIt.next();
            edIt.remove();
        }
        // trying to remove all vertexes.
        Iterator<Vertex<String>> vertIt = vertices.iterator();
        while (vertIt.hasNext()) {
            vertIt.next();
            vertIt.remove();
        }
        // graph after allegedly "removes"
        String afterRemove = graph.toString();
        assertEquals(original, afterRemove);        
    }

    @Test
    public void unchangedOutgoingIterable() {
        Vertex<String> v1 = graph.insert("A");
        int totalEdges = 50;
        for (int i = 0; i < totalEdges; i++) {
            graph.insert(v1, graph.insert("B"), 1);
        }
        String original = graph.toString();
        Iterable<Edge<Integer>> outgoing = graph.outgoing(v1);
        Iterator<Edge<Integer>> outgoingIt = outgoing.iterator();
        while (outgoingIt.hasNext()) {
            outgoingIt.next();
            outgoingIt.remove();
        }
        String afterRemove = graph.toString();
        assertEquals(original, afterRemove);
    }

    @Test
    public void unchangedIncomingIterable() {
        Vertex<String> v1 = graph.insert("A");
        int totalEdges = 50;
        for (int i = 0; i < totalEdges; i++) {
            graph.insert(v1, graph.insert("B"), 1);
        }
        String original = graph.toString();
        Iterable<Edge<Integer>> incoming = graph.incoming(v1);
        Iterator<Edge<Integer>> incomingIt = incoming.iterator();
        while (incomingIt.hasNext()) {
            incomingIt.next();
            incomingIt.remove();
        }
        String afterRemove = graph.toString();
        assertEquals(original, afterRemove);
    }
}
javac -Xlint:all Graph.java
javac -Xlint:all SparseGraph.java

javac -Xlint:all -classpath /usr/share/java/junit4.jar:. SparseGraphTest.java
java -classpath /usr/share/java/junit4.jar:. org.junit.runner.JUnitCore SparseGraphTest

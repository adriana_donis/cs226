import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Arrays;

/** Class to decode a BWT back to the original string. */
public final class BWTDecoder {

    /** Make checkstyle happy. */
    private BWTDecoder() { }

    /** Decode the BWT of a string.
        @param bwt the input bwt string
        @return The original string (before BWT)
    */
    public static String decodeBWT(String bwt) throws IOException {
        if (bwt == null || bwt.length() == 0) {
            throw new IOException("Can't decode empty string");
        }

        StringBuilder text = new StringBuilder();
        String first = makeFirstColumn(bwt);
        if (first.charAt(0) != '$' || first.charAt(1) == '$') {
            throw new IOException("More than one $");
        }
        int row = 0;
        Character ch = bwt.charAt(row);
        while (ch != '$') {
            if (ch == ' ' || ch == '@' || ch == '!'
                || ch == '"' || ch == '#') {
                throw new IOException("Can't handle special characters");
            }
            text.append(ch);
            row = applyLF(first, ch, rank(bwt, row));
            ch = bwt.charAt(row);
        }
        return text.reverse().toString();
    }

    private static int rank(String bwt, int row) {
        Character c = bwt.charAt(row);
        int rank = 0;
        for (int i = 0; i <= row; i++) {
            if (c.equals(bwt.charAt(i))) {
                rank++;
            }
        }
        return rank;
    }

    private static int applyLF(String first, Character c, int rank) {
        int counter = 0;
        for (int i = 0; i < first.length(); i++) {
            if (c.equals(first.charAt(i))) {
                counter++;
                if (counter == rank) {
                    return i;
                }
            }
        }
        return 0;
    }

    private static String makeFirstColumn(String bwt) {
        char[] arr = bwt.toCharArray();
        Arrays.sort(arr);
        return new String(arr);
    }

    /** Reads the input data into a string.
        @return A string with the input data wiht newlines removed
        @throws IOException On error reading input
    */
    public static String readInput() throws IOException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(input);
        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();
        input.close();

        return sb.toString();
    }

    /** Reads a BWT string from standard in and returns the original string.
        @param args ignored
    */
    public static void main(String[] args) {
        String bwt = "";

        try {
            bwt = readInput();
        } catch (IOException e) {
            System.err.println("Cant read input");
            System.exit(1);
        }

        try {
            System.out.println(BWTDecoder.decodeBWT(bwt));
        } catch (IOException e) {
            System.err.println("Invalid input");
            System.exit(1);
        }
    }
}

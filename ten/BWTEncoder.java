/**
* Adriana Donis Noriega
* adonisn1@jhu.edu
* HW 10
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;


/**
  Builds the BWT of a string.

  Builds the BWT by sorting all of the cyclic permutations.
**/
public class BWTEncoder {

    // You may want to use this ;-)
    private static class CyclicPermutation
        implements Comparable<CyclicPermutation> {

        //private String perm;
        Integer index;
        int length;

        CyclicPermutation(Integer suff, int l) {
            this.index = suff;
            this.length = l;
        }

        public Character lastChar() {
            if (this.index == 0) {
                return BWTEncoder.text.charAt(this.length - 1);
            }
            //System.out.println(BWTEncoder.text.charAt((this.index - 1) % length - 1));
            return BWTEncoder.text.charAt(this.index - 1);
        }

        public int compareTo(CyclicPermutation other) {
            int ind1 = this.index;
            int ind2 = other.index;
            Character one = 'a';
            Character two = 'b';
            while (one != '$') {
                one = BWTEncoder.text.charAt(ind1++);
                two = BWTEncoder.text.charAt(ind2++);
                int comparison = one.compareTo(two);
                if (comparison != 0) {
                    return comparison;
                }  
            }
            return -1;
        }
    }

    private static String text;

    /** Silence checkstyle. */
    public BWTEncoder() { }

    /** Construct the BWT of a string.

       Derives the list of possible permutations, sorts them, and then
       extracts the last colunm

       @param input The input String
       @return The BWT of the input string
    */
    public String createBWT(String input) throws IOException {
        if (input == null) {
            throw new IOException("Can't encode null");
        }
        BWTEncoder.text = input + "$";
        int length = BWTEncoder.text.length();
        CyclicPermutation[] array = new CyclicPermutation[BWTEncoder.text.length()];
        StringBuilder bwt = new StringBuilder();
        for (int i = 0; i < BWTEncoder.text.length(); i++) {
            array[i] = new CyclicPermutation(i, BWTEncoder.text.length());
        }
        Arrays.sort(array);
        bwt = new StringBuilder();
        for (CyclicPermutation c : array) {
            Character ch = c.lastChar();
            if ((int) ch < (int)'$' || ch == ' ' || ch == '@' || ch == '!'
                || ch == '"' || ch == '#') {
                throw new IOException("Character is smaller than $");
            }
            bwt.append(ch);
        }
        return bwt.toString();
    }

    /** Reads the input from standard in.
        @return Returns a string with the input data without newlines
        @throws IOException if the file cannot be read
    */
    public static String readInput() throws IOException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(input);
        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();
        input.close();

        return sb.toString();
    }

    /** Main method, load file into string, compute BWT.
        @param args Input arguments, ingorned
    */
    public static void main(String[] args) {
        String text = "";

        try {
            text = readInput();
        } catch (IOException e) {
            System.err.println("Cant read input");
            System.exit(1);
        }

        BWTEncoder bwt = new BWTEncoder();
        try {
            System.out.println(bwt.createBWT(text));
        } catch (IOException e) {
            System.err.println("Invalid Input");
            System.exit(1);
        }
    }
}

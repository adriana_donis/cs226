/**
* @author Adriana Donis Noriega
* adonisn1@jhu.edu
* HW8
*/

import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
* Tests for bwt encoder and decoder. 
* 
*/
public class BWTTests {

    BWTEncoder bwt = new BWTEncoder();

    @Test
    public void emptyEncode() throws IOException {
        String s = "";
        assertEquals("$", bwt.createBWT(s));
    }

    @Test
    public void testEncode() throws IOException {
        String s = "GATTACA";
        assertEquals("ACTGA$TA", bwt.createBWT(s));
    }

    @Test
    public void testDecode() throws IOException {
        String s = "ACTGA$TA";
        String output = "";
        output = BWTDecoder.decodeBWT(s);
        assertEquals("GATTACA", output);
    }

    @Test
    public void encodeDecode() throws IOException {
        String s = "ADRIANA";
        assertEquals(s, BWTDecoder.decodeBWT(bwt.createBWT(s)));
    }

    @Test
    public void testEncode2() throws IOException {
        String s = "BANANA";
        String expected = "ANNB$AA";
        assertEquals(expected, bwt.createBWT(s));
        assertEquals(s, BWTDecoder.decodeBWT(expected));
    }

    @Test
    public void bensExample() throws IOException {
        String s = "Tomorrow_and_tomorrow_and_tomorrow";
        String expected = "w$wwdd__nnoooaattTmmmrrrrrrooo__ooo";
        assertEquals(expected, bwt.createBWT(s));
        assertEquals(s, BWTDecoder.decodeBWT(expected));
    }

    @Test
    public void bensExample2() throws IOException {
        String s = "It_was_the_best_of_times_it_was_the_worst_of_times";
        String expected = "s$esttssfftteww_hhmmbootttt_ii__woeeaaressIi_______";
        assertEquals(expected, bwt.createBWT(s));
        assertEquals(s, BWTDecoder.decodeBWT(expected));
    }

    @Test
    public void bensExample3() throws IOException {
        String s = "in_the_jingle_jangle_morning_Ill_come_following_you";
        String expected = "u_gleeeengj_mlhl_nnnnt$nwj__lggIolo_iiiiarfcmylo_oo_";
        assertEquals(expected, bwt.createBWT(s));
        assertEquals(s, BWTDecoder.decodeBWT(expected));
    }

    @Test(expected = IOException.class)
    public void decodeEmpty() throws IOException {
        String s = "";
        BWTDecoder.decodeBWT(s);
    }

    @Test(expected = IOException.class)
    public void decodeNull() throws IOException {
        BWTDecoder.decodeBWT(null);
    }

    @Test(expected = IOException.class)
    public void decodeMoreThanOneDollar() throws IOException {
        String s = "ABCD$AAA$";
        BWTDecoder.decodeBWT(s);
    }

    @Test(expected = IOException.class)
    public void decodeNoDollar() throws IOException {
        String s = "ABCD";
        BWTDecoder.decodeBWT(s);
    }

    @Test(expected = IOException.class)
    public void encodeNull() throws IOException {
        bwt.createBWT(null);
    }

    @Test(expected = IOException.class)
    public void encodeSmallerThanDollar() throws IOException {
        System.out.println(bwt.createBWT("aaa@!/'"));
    }

    
}
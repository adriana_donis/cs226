/**
* Adriana Donis Noriega
* adonisn1@jhu.edu
* HW 10
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
  Computes a Run Length Encoding of a string.
**/
public final class RLEDecoder {

    /** Make checkstyle happy. */
    private RLEDecoder() { }

    /** Run Length Decode the String.

       @param input The input RLE String
       @return The decoded RLE string
    */
    public static String runLengthDecode(String input) throws IOException {
        if (input == null) {
            throw new IOException("Can't decode null");
        }
        StringBuilder out = new StringBuilder();
        int i = 0;
        while (i < input.length()) {
            Character c = input.charAt(i++);
            int occurrances = 0;
            StringBuilder number = new StringBuilder();
            for (; i < input.length(); i++) {
                try {
                    Integer.parseInt(String.valueOf(input.charAt(i)));
                    number.append(input.charAt(i));
                } catch (NumberFormatException e) {
                    break;
                }
            }
            if (number.toString().equals("")) {
                out.append(c);
                continue;
            } else if (Integer.valueOf(number.toString()) == 0) {
                throw new IOException("Can't have 0 occurrances");
            }
            for (int j = 0; j < Integer.valueOf(number.toString()); j++) {
                out.append(c);
            }
        }

        return out.toString();
    }

    private static int readOccurances(String input, int index) {
        int total = 0;
        int counter = 0;
        StringBuilder number = new StringBuilder();
        for (int i = index; i < input.length(); i++) {
            try {
                Integer.parseInt(String.valueOf(input.charAt(index)));
                number.append(input.charAt(index));
            } catch (NumberFormatException e) {
                return Integer.valueOf(number.toString());
            }
        }
        return Integer.valueOf(number.toString());
    }

    /** Reads the input from standard in.
        @return Returns a string with the input data without newlines
        @throws IOException if the file cannot be read
    */
    public static String readInput() throws IOException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(input);
        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();
        input.close();

        return sb.toString();
    }

    /** Main method, load file into string, compute BWT.
        @param args Input arguments, ingorned
    */
    public static void main(String[] args) {
        String text = "";

        try {
            text = readInput();
        } catch (IOException e) {
            System.err.println("Cant read input");
            System.exit(1);
        }

        try {
            System.out.println(runLengthDecode(text));
        } catch (IOException e) {
            System.err.println("Invalid Input");
            System.exit(1);
        }
    }
}

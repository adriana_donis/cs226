/**
* Adriana Donis Noriega
* adonisn1@jhu.edu
* HW 10
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
  Computes a Run Length Encoding of a string.
**/
public final class RLEEncoder {

    /** Make checkstyle happy. */
    private RLEEncoder() { }

    /** Run Length Encode the String.

       @param input The input String
       @return The run length encoded version of the input string
    */
    public static String runLengthEncode(String input) throws IOException {
        if (input == null) {
            throw new IOException("Can't encode null");
        }
        StringBuilder out = new StringBuilder();
        Character previous = input.charAt(0);
        int instance = 1;
        for (int i = 1; i < input.length(); i++) {
            if (Character.isDigit(previous)) {
                throw new IOException("String contains special characters");
            }
            if (previous.equals(input.charAt(i))) {
                instance++;
            } else {
                out.append(previous);
                String occurrance = Integer.toString(instance);
                if (!occurrance.equals("1")) {
                    out.append(Integer.toString(instance));
                }
                previous = input.charAt(i);
                instance = 1;
            }
        }
        out.append(previous);
        String occurrance = Integer.toString(instance);
        if (!occurrance.equals("1")) {
            out.append(Integer.toString(instance));
        }
        // Your code here :)

        return out.toString();
    }

    /** Reads the input from standard in.
        @return Returns a string with the input data without newlines
        @throws IOException if the file cannot be read
    */
    public static String readInput() throws IOException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(input);
        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();
        input.close();

        return sb.toString();
    }

    /** Main method, load file into string, compute BWT.
        @param args Input arguments, ingorned
    */
    public static void main(String[] args) {
        String text = "";

        try {
            text = readInput();
        } catch (IOException e) {
            System.err.println("Cant read input");
            System.exit(1);
        }

        try {
            System.out.println(runLengthEncode(text));
        } catch (IOException e) {
            System.err.println("Invalid Input");
            System.exit(1);
        }
    }
}

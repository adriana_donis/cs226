/**
* @author Adriana Donis Noriega
* adonisn1@jhu.edu
* HW8
*/

import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
*
*
*/
public class RLETests {

    @Test
    public void onlyOneOccurrance() throws IOException {
        assertEquals("ABC2D2", RLEEncoder.runLengthEncode("ABCCDD"));
    }

    @Test
    public void onlyOneDigit() throws IOException {
        String s = "A5";
        String expected = "";
        for (int i = 0; i < 5; i++) {
            expected += "A";
        }
        assertEquals(expected, RLEDecoder.runLengthDecode(s));
    }

    @Test
    public void small() throws IOException {
        String s = "A10B12";
        String expected = "";
        for (int i = 0; i < 10; i++) {
            expected += "A";
        }
        for (int i = 0; i < 12; i++) {
            expected += "B";
        }
        assertEquals(expected, RLEDecoder.runLengthDecode(s));
    }

    @Test
    public void threeDigits() throws IOException {
        String s = "C105";
        String expected = "";
        for (int i = 0; i < 105; i++) {
            expected += "C";
        }
        assertEquals(expected, RLEDecoder.runLengthDecode(s));
    }

    @Test
    public void encodeOneLetter() throws IOException {
        String s = "AAAAAAAAAAA";
        String coded = RLEEncoder.runLengthEncode(s);
        assertEquals("A11", coded);
    }
    
    @Test
    public void encodeSeveralLetters() throws IOException {
        String s = "AABBBCCCAAABBCDABCD";
        String coded = RLEEncoder.runLengthEncode(s);
        assertEquals("A2B3C3A3B2CDABCD", coded);
    }

    @Test
    public void encodeDecode() throws IOException {
        String s = "AABBBCCCAAABBCDABCD";
        String coded = RLEEncoder.runLengthEncode(s);
        String decode = RLEDecoder.runLengthDecode(coded);
        assertEquals(s, decode);
    }

    @Test
    public void encodeDecodeLong() throws IOException {
        String s = "";
       for (int i = 0; i < 100; i++) {
            s += "A";
        }
        for (int i = 0; i < 1500; i++) {
            s += "B";
        }
        String coded = RLEEncoder.runLengthEncode(s);
        String decoded = RLEDecoder.runLengthDecode(coded);
        assertEquals("A100B1500", coded);
        assertEquals(s, decoded);
    }

    @Test(expected = IOException.class)
    public void encodeNumerals() throws IOException {
        RLEEncoder.runLengthEncode("ABC123");
    }

    @Test(expected = IOException.class)
    public void encodeNull() throws IOException {
        RLEEncoder.runLengthEncode(null);
    }

    @Test(expected = IOException.class)
    public void decodeNull() throws IOException {
        RLEDecoder.runLengthDecode(null);
    }

    @Test(expected = IOException.class)
    public void decodeZero() throws IOException {
        RLEDecoder.runLengthDecode("A2B0");
    }

}
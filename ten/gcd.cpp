#include<iostream>
#include<stdio.h>
using namespace std;

int main() {
	long n = 328419349;
	long e = 220037467;
	long mu = 328366764;
	long c = 5;
	long prevx = 1;
	long prevy = 0;
	long x = 0;
	long y = 1;
	long q;
	long temp;
	while (e != 0) {
		c = mu % e;
		q = mu / e;
		cout << "gcd(" << mu << "," << e << ")\t\t";
		printf("%ld = %ld(%ld) + %ld\n", mu, e, q, c);
		temp = x;
		x = prevx - q*x;
		prevx = temp;
		temp = y;
		y = prevy - q*y;
		prevy = temp;
		//cout << prevx << " " << prevy << endl;
		mu = e;
		e = c;
	}
	cout << "X is: " << prevx << " Y is: " << prevy << endl;

	return 0;
}